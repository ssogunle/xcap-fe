/**
 * "SchemaValidator.java (c) Fall 2005 Columbia University"
 */
package com.inted.xcap.fe.parsers;

import org.w3c.dom.ls.LSInput;
import org.w3c.dom.ls.LSResourceResolver;
import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;

import java.io.File;
import java.io.ByteArrayInputStream;

import javax.servlet.http.HttpServletResponse;
import com.inted.xcap.fe.util.XCAPException;

import org.w3c.dom.Node;

import com.inted.xcap.fe.util.SchemaConstants;
import com.inted.xcap.fe.error.*;
import java.io.StringReader;


/** SchemaValidator the use of jaxp validation apis. */
public class SchemaValidator
{

  /**
   * Method: SchemaValidator
   *         Constructor to construct this object for a particular
   *         validation context.
   * @param  validationContext, specifies the context of validation.
   *         document  --> validate whole xml as per the schema specified edu.columbia.xcap.error.
   *                       in the validateSchema function.
   *         element   --> validate xml fragment as per the schema specified
   *                       in the validateSchema function
   *         attribute --> validate as per traditional "att_name:att_value"
   *                       grammer.
   * @note   document and element are validated the same way based on the
   *         xml schema. The direct repurcussion of this is that every
   *         element in the xml schema for the application usage needs to
   *         have every element defined at the root level.
   *         This allows the same old parsers to work of xml-fragment
   *         validation.
   */
  public SchemaValidator(String validationContext)
  { 
    _validationContext = validationContext;
    _error = false;
    _message = new String("default");
  }; // validationContext

  
  public void setValidationContext(String validationContext)
  {
    _validationContext = validationContext;
  }; // setValidationContext


  /**
   * Method: throwException
   *         Method to throw different kind of exception based on the
   *         validateType of the class.
   * @param  message specifies the exception message
   */
  private void throwException(String message)
    throws XCAPException
  {
    System.out.println("_message: " + message);
    if(_validationContext == SchemaConstants.DOCUMENT) 
    {
      throw new XCAPException(HttpServletResponse.SC_CONFLICT, 
           new NotWellFormed(SchemaConstants.NULLSCHEMA, message));
    }
    else if(_validationContext == SchemaConstants.ELEMENT)
    {
      throw new XCAPException(HttpServletResponse.SC_CONFLICT, 
           new NotXMLFrag(SchemaConstants.NULLSCHEMA, message));
    }
    else if(_validationContext == SchemaConstants.ATTRIBUTE)
    {
      throw new XCAPException(HttpServletResponse.SC_CONFLICT, 
           new NotXMLAttValue(SchemaConstants.NULLSCHEMA, message));
    }
    else if(_validationContext == SchemaConstants.WHOLE)
    {
      throw new XCAPException(HttpServletResponse.SC_CONFLICT,
          new SchemaValidationError(SchemaConstants.NULLSCHEMA, message));
    }
  }; // throwException

 
  /** A custom SAX error handler */
  protected class Handler implements ErrorHandler 
  {
    public void error(SAXParseException ex) 
      //throws XCAPException
    {
      System.out.println("%%%" + ex.getMessage()+ " At line:" + ex.getLineNumber());
      _message = ex.getMessage() + " At line:" + ex.getLineNumber();
      _error = true;
      //throwException("error: " + ex.getLineNumber() + ":" + ex.getMessage());
    }; // error


    public void fatalError(SAXParseException ex) 
      // throws XCAPException
    {
      System.out.println("%%%%%%%" + ex.getMessage()+ " At line:" + ex.getLineNumber());
      _message = ex.getMessage() + " At line:" + ex.getLineNumber();
      _error = true;
      //throwException("error: " + ex.getLineNumber() + ":" + ex.getMessage());
    }; // fatalError


    public void warning(org.xml.sax.SAXParseException ex) 
      //throws XCAPException
    {
      System.out.println("********" + ex.getMessage()+ " At line:" + ex.getLineNumber());
      _message = ex.getMessage() + " At line:" + ex.getLineNumber();
      _error = true;
      //throwException("warning: " + ex.getLineNumber() + ":" + ex.getMessage());
    }; // warning

  }; // class Handler


  /**
   * Inner class to implement a resource resolver. This version always returns
   * null, which has the same effect as not supplying a resource resolver at 
   * all. The LSResourceResolver is part of the DOM Level 3 load/save module.
   */
  protected static class Resolver implements LSResourceResolver
  {

    /**
     * Method: resolveResource
     *         Resolve a reference to a resource
     * @param  type The type of resource, for example a schema, source XML 
     *         document or query
     * @param  namespace The target namespace (in the case of a schema 
     *         document)
     * @param  publicId The public ID
     * @param  systemId The system identifier (as written, possibly a 
     *         relative URI)
     * @param  baseURI The base URI against which the system identifier 
     *         should be resolved
     * @return an LSInput object typically containing the character stream or 
     *         byte stream identified by the supplied parameters; or null if 
     *         the reference cannot be resolved or if the resolver chooses not
     *         to resolve it.
     */
    public LSInput resolveResource(String type, String namespace, 
        String publicId, String systemId, String baseURI) 
    {
        return null;
    }; // LSInput

  }; // inner class Resolver


  /**
   * Method: validateSchema
   *         Validate the xml provided with the given schema
   * @param  schemaFile the xml schema file
   * @param  xmlDoc the xml to be validated
   * @throws XCAPException
   */
  public void validateSchema(String schemaFile, String xmlDoc)
    throws XCAPException
  {
    System.out.println("schemaFile: " + schemaFile);
    if((_validationContext == SchemaConstants.DOCUMENT) || 
        (_validationContext == SchemaConstants.ELEMENT) ||
        (_validationContext == SchemaConstants.WHOLE))
    {
      try
      {
        Handler handler = new Handler();

        SchemaFactory schemaFactory = SchemaFactory.newInstance(
            SchemaConstants.W3C_SCHEMALOC);
        System.err.println("Loaded schema validation provider " + 
            schemaFactory.getClass().getName());

        schemaFactory.setErrorHandler(handler);

        System.err.println("Schema File Location: " + schemaFile);
        System.err.println("xmlDoc: " + xmlDoc);
        File schemaFileLoc = new File(schemaFile);
        if(schemaFileLoc.exists() == false)
        {
          throw new XCAPException(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, 
              new NullError(SchemaConstants.NULLSCHEMA,
                "Application Usage does not define a schema file"));
        }

        Schema schemaGrammar = schemaFactory.newSchema(schemaFileLoc);
        System.err.println("Created Grammar object for schema : "+schemaFile);

        Resolver resolver = new Resolver();
        Validator schemaValidator = schemaGrammar.newValidator();
        schemaValidator.setResourceResolver(resolver);
        schemaValidator.setErrorHandler(handler);
        
        schemaValidator.validate(new StreamSource(new StringReader(xmlDoc)));//new ByteArrayInputStream(xmlDoc.getBytes())));
        if(_error)
        {
          throwException(_message);
        }
      }
      catch(SAXException exception) 
      {
        System.out.println("SAX");
        exception.printStackTrace();
        throwException(exception.getMessage());
      }
      catch(java.io.IOException exception)
      {
        System.out.println("IO");
        throwException(exception.getMessage());
      }
    }
    else if(_validationContext == SchemaConstants.ATTRIBUTE)
    {
      String att[] = xmlDoc.split(":");
      if(att.length != 2)
      {
        throwException("Invalid Attribute syntax in body");
      }
    }
    else
    {
      throw new XCAPException(HttpServletResponse.SC_INTERNAL_SERVER_ERROR,
          new NullError(SchemaConstants.NULLSCHEMA, 
            "xml fragment type not supported"));
    }

  }; // validateSchema


  /**
   * Method: validateSchema
   *         Validate the xml provided with the given schema
   * @param  schemaFile the xml schema file
   * @param  xmlDoc the xml to be validated
   * @throws XCAPException
   */
  public void validateSchema(String schemaFile, Node xmlDoc)
    throws XCAPException
  {
    String xmlToValidate = SaxonParser.print(xmlDoc, "test");
    validateSchema(schemaFile, xmlToValidate);
  }; // validateSchema


  /** 
   * Tell this class whether to validate for a document, element or
   * attribute
   */
  private String _validationContext;
  private String _message;
  private boolean _error;

}; // SchemaValidator
