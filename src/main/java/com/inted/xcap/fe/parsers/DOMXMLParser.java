/**
 * "DOMXMLParser.java (c) Fall 2005 Columbia University"
 */

/** DOMXMLParser package */
package com.inted.xcap.fe.parsers;

/** jaxp imports */
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

/** dom node, namespace imports */
import org.w3c.dom.Node;
import org.w3c.dom.bootstrap.DOMImplementationRegistry;
import org.xml.sax.InputSource;

import com.inted.xcap.fe.error.NotWellFormed;
import com.inted.xcap.fe.util.XCAPException;

import java.io.InputStream;
import com.inted.xcap.fe.util.SchemaConstants;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;




/**
 * No pooling on parsers till now
 * Parsers are not thread safe.
 * Class to aggregate the DOM parser nitty gritties
 * Also helps to set the parser variables in one place otherwise is a pain
 */
public class DOMXMLParser
{

  /**
   * Method: DOMXMLParser
   *         Contructor to set the parser settinga
   * @param  nameSpaceAware boolean value indicating whether the parser to
   *         be used needs to be namespace aware or not. true means the
   *         parser is namespace aware
   * @throws XCAPException        
   */
  public DOMXMLParser(boolean nameSpaceAware)
    throws XCAPException
  {
    try
    {
      System.setProperty(DOMImplementationRegistry.PROPERTY, 
           "org.apache.xerces.dom.DOMImplementationSourceImpl");
      DocumentBuilderFactory dfactory = DocumentBuilderFactory.newInstance();
      dfactory.setNamespaceAware(true);
      dfactory.setIgnoringElementContentWhitespace(nameSpaceAware);
      dfactory.setCoalescing(true);
      dfactory.setIgnoringComments(false);
      docBuilder = dfactory.newDocumentBuilder(); 
    }
    catch(javax.xml.parsers.ParserConfigurationException parserConfigError)
    {
      throw new XCAPException(HttpServletResponse.SC_INTERNAL_SERVER_ERROR,
          new NotWellFormed(SchemaConstants.NULLSCHEMA, 
            parserConfigError.getMessage()));
    }
  }; // DOMXMLParser


  /**
   * Method: parse
   *         Method to parse a string into DOM document
   * @param  inputStream input stream to be parsed
   * @return Node specifies the Node formed
   * @throws XCAPException
   */
  public Node parse(InputStream inputStream)
    throws XCAPException
  {
    Node node = null;
    try
    {
      node = docBuilder.parse(new InputSource(inputStream));
    }
    catch(org.xml.sax.SAXException saxException)
    {
      throw new XCAPException(HttpServletResponse.SC_CONFLICT,
          new NotWellFormed(SchemaConstants.NULLSCHEMA,
          saxException.getMessage()));
    }
    catch(java.io.IOException exp)
    {
      throw new XCAPException(HttpServletResponse.SC_CONFLICT,
          new NotWellFormed(SchemaConstants.NULLSCHEMA,
          exp.getMessage()));
    }
    return node;
  }; // parse


  /** JAXP Parser */
  DocumentBuilder docBuilder;

}; // class DOMXMLParser
