/**
 * "SaxonParser.java (c) Fall 2005 Columbia University"
 */

/** Utils package */
package com.inted.xcap.fe.parsers;

import java.lang.Exception;

/** jaxp imports */
/** xpath imports */
import javax.xml.xpath.XPathFactory;      // jaxp spec
import javax.xml.transform.sax.SAXSource; // Source for setting XPATHEvaluator
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.namespace.NamespaceContext;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.XMLConstants;
import javax.xml.transform.Source;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Validator;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import javax.xml.transform.*; // Finally import all only specifics should be
import javax.xml.transform.dom.DOMResult;
import javax.xml.transform.Result;

/** dom node, namespace imports */
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Element;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.ls.LSSerializer;
import org.w3c.dom.ls.DOMImplementationLS;
import org.w3c.dom.bootstrap.DOMImplementationRegistry;
import org.xml.sax.InputSource;


/** saxon imports */
import net.sf.saxon.lib.NamespaceConstant; // For XPathFactory class loader
import net.sf.saxon.om.NodeInfo;
import net.sf.saxon.pull.NamespaceContextImpl;
import net.sf.saxon.sxpath.IndependentContext;
import net.sf.saxon.xpath.XPathEvaluator;
//import net.sf.saxon.sxpath.StandaloneContext;
//import net.sf.saxon.sxpath.NamespaceContextImpl;


/** HTTP response codes */
import javax.servlet.http.HttpServletResponse;

/** Base import */
import com.inted.xcap.fe.parsers.Parser;

/** Application Usage */
import  com.inted.xcap.fe.AppUsages.AppUsage;
import  com.inted.xcap.fe.AppUsages.AppUsageFactory;

/** Exceptions */
import  com.inted.xcap.fe.util.XCAPException;

/** Utils and error includes */
import com.inted.xcap.fe.util.*;
import  com.inted.xcap.fe.error.*;


/** java file */
import java.io.File;
import java.io.InputStream;
import java.io.StringReader;
import java.io.FileInputStream;
import java.io.ByteArrayInputStream;


/** Validators imports */
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;

/** SAX imports */
import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

/** StringReader import */
import java.io.StringReader;

/** Output Stream imports */
import java.io.ByteArrayOutputStream;

/** List imports */
import java.util.List;
import java.util.Iterator;

import java.util.Properties;


/** Saxon imports */
import net.sf.saxon.query.QueryResult;
import net.sf.saxon.Configuration;

//import edu.columbia.xcap.parser.SchemaValidator;


/**
 * Class forms a base class for different parser implementations
 */
public class SaxonParser implements Parser
{
Logger LOG = LoggerFactory.getLogger(SaxonParser.class);

  /**
   * Method: evaluateXPATH
   *         Method to evaluate XPATH expression and return back the string
   *         found
   * @param  xpathExpr specifies the xpathExpr to be evaluated
   * @param  query specifies the namespaces to be bound to the
   *         xpathExpression.
   * @param  doc specifies the DOM Node on which query has to be run
   * @param  AppUsage specifies the AppUsage for this get call
   * @return NodeList specifies the final result of the xpath query
   * @note   namespace attributes (such as xmlns) cannot be selected.
   * @note   As a result, once the entire node selector is evaluated against 
   *         the document, the result will either be a no-match, invalid, or 
   *         a single element or single attribute. 
   */
  public NodeList evaluateXPATH(String xpathExpr, String query, Node doc, 
      AppUsage appUsage) 
    throws XCAPException
  {
    System.out.println("xpathExpr: " + xpathExpr);
    NodeList nodeList = null;
    List list = null;
    try
    {
     
      XPathFactory xpathF = 
        XPathFactory.newInstance(XPathConstants.DOM_OBJECT_MODEL);
      XPath xpathE = xpathF.newXPath();
      
      IndependentContext context = new IndependentContext();
      
      if((appUsage.getDefaultNamespaceName().equals("") == false) &&
         (appUsage.getDefaultNamespaceValue().equals("") == false))
      {
        System.out.println("appUsage.getDefaultNamespaceName : " + appUsage.getDefaultNamespaceName());
        System.out.println("appUsage.getDefaultNamespaceValue: " + appUsage.getDefaultNamespaceValue());
        context.declareNamespace(appUsage.getDefaultNamespaceName(), 
            appUsage.getDefaultNamespaceValue());
      }

      if(query.equals("") == false)
      {
        System.out.println("Splitting query: " + query);
        String namespace[] = query.split("\\)");
        System.out.println("Splitted query parts: " + namespace.length);

        for(int index=0; index<namespace.length; index++)
        {
          namespace[index] =  namespace[index] + ")";
          System.out.println("Splitted query to " + namespace.length + 
              " parts");
          String temp[] = namespace[index].split("=");
          if(temp.length == 2)
          {
            System.out.println("part 1" + temp[0]);
            System.out.println("part 2" + temp[1]);
            int indexOfOpenBraces = temp[0].indexOf("(");
            int indexOfOpenQuotes = temp[1].indexOf("\"");
            int indexOfCloseQoutes = temp[1].indexOf("\"", indexOfOpenQuotes+1);

            String namespaceTag = temp[0].substring(indexOfOpenBraces+1);
            String namespaceValue = temp[1].substring(indexOfOpenQuotes+1, 
                indexOfCloseQoutes);

            System.out.println("namespaceTag: " + namespaceTag);
            System.out.println("namespaceValue: " + namespaceValue);

            context.declareNamespace(namespaceTag, namespaceValue);
          }
          else
          {
            throw new XCAPException(HttpServletResponse.SC_CONFLICT, 
                new NullError(SchemaConstants.NULLSCHEMA, "Query given wrong"));
          }
        }
      }

      NamespaceContext namespaceContext = new NamespaceContextImpl(context);
      xpathE.setNamespaceContext(namespaceContext);
 
      System.out.println("In SAxonParser xpathExpr: " + xpathExpr);
      XPathExpression xpathExpression = xpathE.compile(xpathExpr);
      nodeList = (NodeList)xpathExpression.evaluate((Object)doc, 
          XPathConstants.NODESET);
    }
    catch(javax.xml.xpath.XPathFactoryConfigurationException exp)
    {
      throw new XCAPException(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, 
          new NullError(SchemaConstants.NULLSCHEMA, "xpath factory error"));
    }
    catch(javax.xml.xpath.XPathExpressionException exp)
    {
      throw new XCAPException(HttpServletResponse.SC_NOT_FOUND, 
          new NullError(SchemaConstants.NULLSCHEMA, "wrong xpath expression"));
    }

    return nodeList;
  }; // evaluateXPATH


  /**
   * Method: print
   *         Static method to serialize the DOMNode with message to be printed
   *         Uses javax.Transformer to serialize
   * @param  nodeToBePrinted DOM Node to be printed
   * @param  msg String to be friendly string to be outputted as console
   * @return String the serialized DOM Node
   * @throws XCAPException
   */
  public static String print(Node nodeToBePrinted, String msg)
    throws XCAPException
  {
    System.out.println("-->SaxonParser::print");
    String result = null;

    System.out.println("************************"+msg+"***********************");
    try
    {
      TransformerFactory tfactory = TransformerFactory.newInstance(); 
      Transformer serializer = tfactory.newTransformer();
      serializer.setOutputProperty(javax.xml.transform.OutputKeys.INDENT,"yes");
      Source source = new DOMSource(nodeToBePrinted);
      ByteArrayOutputStream baout = new ByteArrayOutputStream();
      StreamResult streamResult = new StreamResult(baout);
      serializer.transform(source, streamResult);
      result = baout.toString();
      System.out.println(result);
    }
    catch(javax.xml.transform.TransformerConfigurationException tcException)
    {
      System.out.println("TException transforming: " +tcException.getMessage());
      throw new XCAPException(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, 
          new NullError(SchemaConstants.NULLSCHEMA, tcException.getMessage()));
    }
    catch(javax.xml.transform.TransformerException tException)
    {
      System.out.println("TException transforming: " + tException.getMessage());
      throw new XCAPException(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, 
          new NullError(SchemaConstants.NULLSCHEMA, tException.getMessage()));
    }
    return result;
  }; // print

  /**
   * Method: print
   *         Static method to serialize the DOMNode with message to be printed
   *         Uses javax.Transformer to serialize
   * @param  nodeToBePrinted DOM Node to be printed
   * @param  msg String to be friendly string to be outputted as console
   * @return String the serialized DOM Node
   * @throws XCAPException
   */
  public static String printFile(Node nodeToBePrinted, String msg)
    throws XCAPException
  {
    System.out.println("-->SaxonParser::print");
    String result = null;

    System.out.println("***********************"+msg+"***********************");
    try
    {
      TransformerFactory tfactory = TransformerFactory.newInstance(); 
      Transformer serializer = tfactory.newTransformer();
      serializer.setOutputProperty(javax.xml.transform.OutputKeys.INDENT, "yes");
      Source source = new DOMSource(nodeToBePrinted);
      ByteArrayOutputStream baout = new ByteArrayOutputStream();
      StreamResult streamResult = new StreamResult(baout);
      serializer.transform(source, streamResult);
      result = baout.toString();
      System.out.println(result);
    }
    catch(javax.xml.transform.TransformerConfigurationException tcException)
    {
      System.out.println("TException transforming: " +tcException.getMessage());
      throw new XCAPException(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, 
          new NullError(SchemaConstants.NULLSCHEMA, tcException.getMessage()));
    }
    catch(javax.xml.transform.TransformerException tException)
    {
      System.out.println("TException transforming: " + tException.getMessage());
      throw new XCAPException(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, 
          new NullError(SchemaConstants.NULLSCHEMA, tException.getMessage()));
    }
    return result;
  }; // printFile


  /**
   * Method: evaluateNamespaces
   *         Method to evaluate XCAP expression of form namespace::* and return
   *         back the namespaces found
   * @param  xpathExpr specifies the xpathExpr to be evaluated
   * @param  query specifies the namespaces to be bound to the
   *         xpathExpression.
   * @param  xml is the the file to be read
   * @param  AppUsage specifies the AppUsage for this get call
   * @return String specifies the namespaces found
   * @note   namespace attributes (such as xmlns) cannot be selected.
   * @note   As a result, once the entire node selector is evaluated against 
   *         the document, the result will either be a no-match, invalid, or 
   *         a single element or single attribute. 
   */
  public String evaluateNamespaces(String xpathExpr, String xpathQuery, String xml, 
      AppUsage appUsage) 
    throws XCAPException
  {
    String result = null;
    try
    {
   // 	DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
	//	DocumentBuilder builder = factory.newDocumentBuilder();
	//	Node node = builder.parse(new InputSource(new StringReader(xml)));
		
      XPathFactory xpathF = XPathFactory.newInstance(NamespaceConstant.OBJECT_MODEL_SAXON);
      XPath xpathE = xpathF.newXPath();
      System.err.println("Loaded XPath Provider " + xpathE.getClass().getName());

      //THIS IS WHERE THE XML STRING IS INSERTED
      InputSource is = new InputSource(new StringReader(xml));
   //   xpathE.
      
      SAXSource ss = new SAXSource(is);
      NodeInfo nodeInfo = ((XPathEvaluator)xpathE).setSource(ss);
      
      IndependentContext context = new IndependentContext();
      
      if((appUsage.getDefaultNamespaceName().equals("") == false) &&
         (appUsage.getDefaultNamespaceValue().equals("") == false))
      {
        LOG.info("appUsage.getDefaultNamespaceName : " + 
            appUsage.getDefaultNamespaceName());
        LOG.info("appUsage.getDefaultNamespaceValue: " + 
            appUsage.getDefaultNamespaceValue());
        context.declareNamespace(appUsage.getDefaultNamespaceName(), 
            appUsage.getDefaultNamespaceValue());
      }

      if(xpathQuery.equals("") == false)
      {
        String namespace[] = xpathQuery.split("xmlns([a-zA-Z0-9]*=\"[:\\-a-zA-Z0-9]+)");
        for(int index=0; index<namespace.length; index++)
        {
          String temp[] = namespace[index].split("=");
          if(temp.length == 2)
          {
            int indexOfOpenBraces = temp[0].indexOf("(");
            int indexOfOpenQuotes = temp[1].indexOf("\"");
            int indexOfCloseQoutes = temp[1].indexOf("\"", indexOfOpenQuotes+1);

            String namespaceTag = temp[0].substring(indexOfOpenBraces+1);
            String namespaceValue = temp[1].substring(indexOfOpenQuotes, 
                indexOfCloseQoutes);

            context.declareNamespace(namespaceTag, namespaceValue);
          }
          else
          {
            throw new XCAPException(HttpServletResponse.SC_CONFLICT, 
                new NullError(SchemaConstants.NULLSCHEMA, "Query given wrong"));
          }
        }
      }

      NamespaceContext namespaceContext = new NamespaceContextImpl(context);
      xpathE.setNamespaceContext(namespaceContext);
      
      //System.err.println("*******AppUsage.BASE+path: " + AppUsage.BASE+xml);
      
      LOG.info("xpathExpr in namespace evaluation: " + xpathExpr); 
      
      XPathExpression findLine = xpathE.compile(xpathExpr);
      
      List matchedLines = (List)findLine.evaluate(nodeInfo, XPathConstants.NODESET);

      if(matchedLines != null) 
      {
        for (Iterator iter = matchedLines.iterator(); iter.hasNext();) 
        {
          NodeInfo line = (NodeInfo)iter.next();

          Result destination = new DOMResult();
          Properties props = new Properties();
          Configuration conf = new Configuration();
          QueryResult.serialize(line, destination, props);
          DOMResult newDom = (DOMResult)destination;
          Node newNode = newDom.getNode();
          result = SaxonParser.print(newNode, "NewNode");
        }
      }
    }
    catch(javax.xml.xpath.XPathFactoryConfigurationException exp)
    {
    	LOG.info("XPathFactoryConfigurationException");
    }
    catch(javax.xml.xpath.XPathExpressionException exp)
    {
    	LOG.info("XPathExpressionException");
    }
    catch(net.sf.saxon.trans.XPathException exp)
    {
    	LOG.info("net.sf.saxon.trans.XPathException" + exp.getCause());
    }
    catch(java.lang.UnsupportedOperationException exp)
    {
    	LOG.info("java.lang.UnsupportedOperationException" + exp.getCause());
    }
    return result;
  }; // evaluateNamespaces

}; // class SaxonParser
