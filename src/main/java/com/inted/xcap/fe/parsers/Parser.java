/**
 * "Parser.java (c) Fall 2005 Columbia University"
 */

/** Parser package */
package com.inted.xcap.fe.parsers;

import com.inted.xcap.fe.AppUsages.AppUsage;

/** Exceptions */
import com.inted.xcap.fe.util.XCAPException;

/**
 * Class forms a abstract base class for different parser implementations
 */
public interface Parser 
{
}; // interface Parser
