/*
 * "NoParent.java (c) Fall 2005 Columbia University"
 */

/* Servlet package */
package com.inted.xcap.fe.error;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

/*
 * NoParent base class
 */
public class NoParent extends ErrorCodes
{
  // --------------------------------------------------------------------------
  /*
   * Method: NoParent
   *         Constructor to initialize base class
   * @param  schemaLocation SchemaLocation for this error code
   * @param  reason descritive reason for this error code
   * <xs:element name="no-parent" substitutionGroup="error-element">
   *   <xs:annotation>
   *     <xs:documentation>
   *       This indicates that an attempt to insert an element, attribute or 
   *       document failed because the document or element into which the 
   *       insertion was supposed to occur does not exist
   *     </xs:documentation>
   *   </xs:annotation>
   *   <xs:complexType>
   *     <xs:sequence>
   *       <xs:element name="ancestor" type="xs:anyURI" minOccurs="0">
   *         <xs:annotation>
   *           <xs:documentation>
   *             Contains an HTTP URI that points to the element which is the
   *             closest ancestor that does exist.
   *           </xs:documentation>
   *         </xs:annotation>
   *       </xs:element>
   *     </xs:sequence>
   *     <xs:attribute name="phrase" type="xs:string" use="optional"/>
   *   </xs:complexType>
   * </xs:element>
   */
  public NoParent(String schemaLocation, String reason)
  {
    super(schemaLocation, reason);
    if(_error == false)
    {
      _ancestor = _document.createElement("ancestor"); 
      _ancestor.setAttribute("phrase", reason);
      _root.appendChild(_ancestor);
    }
  }; // NoParent


  //---------------------------------------------------------------------------
  /**
   * Method: setPhrase
   *         Method to set the phrase attribute of the error code
   * @param  phrase the String phrase to be set
   */
  public void setPhrase(String phrase)
  {
    _ancestor.setAttribute("phrase", phrase);
  }; // setPhrase


  /**
   * Method: getAncestorElement
   *         Method to get the ancestor element of this error tree
   * @return Element specifies the ancestor element
   */
  public Element getAncestorElement()
  { 
    return _ancestor;
  }; // getAncestorElement



  /* Element for this error type */
  private Element _ancestor;

}; // class NoParent
