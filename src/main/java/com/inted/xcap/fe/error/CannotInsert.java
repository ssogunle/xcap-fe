/*
 * "CannotInsert.java (c) Fall 2005 Columbia University"
 */

/* Servlet package */
package com.inted.xcap.fe.error;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

/*
 * CannotInsert base class
 */
public class CannotInsert extends ErrorCodes
{
  // --------------------------------------------------------------------------
  /*
   * Method: CannotInsert
   *         Constructor to initialize base class
   * @param  schemaLocation, SchemaLocation for this error code
   * @param  reason descritive reason for this error code
   * <xs:element name="cannot-insert" substitutionGroup="error-element">
   *   <xs:annotation>
   *     <xs:documentation>
   *       This indicates that the requested PUT operation could not be 
   *       performed because a GET of that resource after the PUT would not 
   *       yield the content of the PUT request.
   *     </xs:documentation>
   *   </xs:annotation>
   *   <xs:complexType>
   *     <xs:attribute name="phrase" type="xs:string" use="optional"/>
   *   </xs:complexType>
   * </xs:element>
   */
  public CannotInsert(String schemaLocation, String reason)
  {
    super(schemaLocation, reason);
    if(_error == false)
    {
      _cannotInsert = _document.createElement("cannot-insert"); 
      _root.appendChild(_cannotInsert);
    }
  }; // CannotInsert


  //---------------------------------------------------------------------------
  /**
   * Method: setPhrase
   *         Method to set the phrase attribute of the error code
   * @param  phrase the String phrase to be set
   */
  public void setPhrase(String phrase)
  {
    _cannotInsert.setAttribute("phrase", phrase);
  }; // setPhrase


  /**
   * Method: getCannotInsertElement
   *         Method to get the cannot-insert element of this error tree
   * @return Element specifies the cannot-insert element
   */
  public Element getCannotInsertElement()
  { 
    return _cannotInsert;
  }; // getCannotInsertElement


  /* Element for this error type */
  private Element _cannotInsert;

}; // class CannotInsert
