/*
 * "NullError.java (c) Fall 2005 Columbia University"
 */

/* Servlet package */
package com.inted.xcap.fe.error;


import org.w3c.dom.Document;
import org.w3c.dom.Element;

/*
 * NullError base class
 */
public class NullError extends ErrorCodes
{
  // --------------------------------------------------------------------------
  /*
   * Method: NullError
   *         Constructor to initialize base class
   *         Has no xml tree to send back
   * @param  schemaLocation SchemaLocation for this error code
   * @param  reason descritive reason for this error code
   */
  public NullError(String schemaLocation, String reason)
  {
    super(schemaLocation, reason);
    _error = true;
    _mimeType = "";
  }; // NullError

}; // class NullError
