/*
 * "SchemaValidationError.java (c) Fall 2005 Columbia University"
 */

/* Servlet package */
package com.inted.xcap.fe.error;

import com.inted.xcap.fe.error.ErrorCodes;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

/*
 * SchemaValidationError base class
 */
public class SchemaValidationError extends ErrorCodes
{
  // --------------------------------------------------------------------------
  /*
   * Method: SchemaValidationError
   *         Constructor to initialize base class
   * @param  schemaLocation SchemaLocation for this error code
   * @param  reason descritive reason for this error code
   * <xs:element name="schema-validation-error" substitutionGroup="error-element">
   *   <xs:annotation>
   *     <xs:documentation>
   *       This element indicates that the document was not compliant to the 
   *       schema after the requested operation was performed.
   *     </xs:documentation>
   *   </xs:annotation>
   *   <xs:complexType>
   *     <xs:attribute name="phrase" type="xs:string" use="optional"/>
   *   </xs:complexType>
   * </xs:element>
   */
  public SchemaValidationError(String schemaLocation, String reason)
  {
    super(schemaLocation, reason);
    if(_error == false)
    {
      _svError = _document.createElement("schema-validation-error"); 
      _svError.setAttribute("phrase", reason);
      _root.appendChild(_svError);
    }
  }; // SchemaValidationError


  //---------------------------------------------------------------------------
  /**
   * Method: setPhrase
   *         Method to set the phrase attribute of the error code
   * @param  phrase the String phrase to be set
   */
  public void setPhrase(String phrase)
  {
    _svError.setAttribute("phrase", phrase);
  }; // setPhrase


  /**
   * Method: getSVErrorElement
   *         Method to get the schema-validation-error element of this error tree
   * @return Element specifies the schema-validation-error element
   */
  public Element getSVErrorElement()
  { 
    return _svError;
  }; // getSVErrorElement



  /* Element for this error type */
  private Element _svError;

}; // class SchemaValidationError
