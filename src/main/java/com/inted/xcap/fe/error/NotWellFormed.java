/*
 * "NotWellFormed.java (c) Fall 2005 Columbia University"
 */

/* Servlet package */
package com.inted.xcap.fe.error;

import com.inted.xcap.fe.error.ErrorCodes;

import java.io.StringWriter;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

//import org.apache.xml.serialize.OutputFormat;
//import org.apache.xml.serialize.XMLSerializer;



/*
 * NotWellFormed base class
 */
public class NotWellFormed extends ErrorCodes
{
  // --------------------------------------------------------------------------
  /*
   * Method: NotWellFormed
   *         Constructor to initialize base class
   * @param  schemaLocation SchemaLocation for this error code
   * @param  reason descritive reason for this error code
   * <xs:element name="not-well-formed" substitutionGroup="error-element">
   *   <xs:annotation>
   *     <xs:documentation>
   *     This indicates that the body of the request was not a well-formed 
   *     document.
   *     </xs:documentation>
   *   </xs:annotation>
   *   <xs:complexType>
   *     <xs:attribute name="phrase" type="xs:string" use="optional"/>
   *   </xs:complexType>
   * </xs:element>
   */
  public NotWellFormed(String schemaLocation, String reason)
  {
    super(schemaLocation, reason);
    if(_error == false)
    {
      _notWellFormed = _document.createElement("not-well-formed"); 
      _notWellFormed.setAttribute("phrase", reason);
      _root.appendChild(_notWellFormed);
    }
  }; // NotWellFormed


  //---------------------------------------------------------------------------
  /**
   * Method: setPhrase
   *         Method to set the phrase attribute of the error code
   * @param  phrase the String phrase to be set
   */
  public void setPhrase(String phrase)
  {
    _notWellFormed.setAttribute("phrase", phrase);
  }; // setPhrase


  /**
   * Method: getNotWellFormedElement
   *         Method to get the not-well-formed element of this error tree
   * @return Element specifies the not-well-formed element
   */
  public Element getNotWellFormedElement()
  { 
    return _notWellFormed;
  }; // getNotWellFormedElement



  /* Element for this error type */
  private Element _notWellFormed;

}; // class NotWellFormed
