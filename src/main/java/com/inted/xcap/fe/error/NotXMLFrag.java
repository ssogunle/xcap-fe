/*
 * "NotXMLFrag.java (c) Fall 2005 Columbia University"
 */

/* Servlet package */
package com.inted.xcap.fe.error;


import org.w3c.dom.Document;
import org.w3c.dom.Element;

/*
 * NotXMLFrag base class
 */
public class NotXMLFrag extends ErrorCodes
{
  // --------------------------------------------------------------------------
  /*
   * Method: NotXMLFrag
   *         Constructor to initialize base class
   * @param  schemaLocation SchemaLocation for this error code
   * @param  reason descritive reason for this error code
   * <xs:element name="not-xml-frag" substitutionGroup="error-element">
   *   <xs:annotation>
   *     <xs:documentation>
   *       This indicates that the request was supposed to contain a valid XML 
   *       fragment body, but did not.
   *     </xs:documentation>
   *   </xs:annotation>
   *   <xs:complexType>
   *     <xs:attribute name="phrase" type="xs:string" use="optional"/>
   *   </xs:complexType>
   * </xs:element>
   */
  public NotXMLFrag(String schemaLocation, String reason)
  {
    super(schemaLocation, reason);
    if(_error == false)
    {
      _notXMLFrag = _document.createElement("not-xml-frag"); 
      _notXMLFrag.setAttribute("phrase", reason);
      _root.appendChild(_notXMLFrag);
    }
  }; // NotXMLFrag


  //---------------------------------------------------------------------------
  /**
   * Method: setPhrase
   *         Method to set the phrase attribute of the error code
   * @param  phrase the String phrase to be set
   */
  public void setPhrase(String phrase)
  {
    _notXMLFrag.setAttribute("phrase", phrase);
  }; // setPhrase


  /**
   * Method: getNotXMLFragElement
   *         Method to get the not-xml-frag element of this error tree
   * @return Element specifies the not-xml-frag element
   */
  public Element getNotXMLFragElement()
  { 
    return _notXMLFrag;
  }; // getNotXMLFragElement



  /* Element for this error type */
  private Element _notXMLFrag;

}; // class NotXMLFrag
