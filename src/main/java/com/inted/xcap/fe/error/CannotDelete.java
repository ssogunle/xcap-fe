/*
 * "CannotDelete.java (c) Fall 2005 Columbia University"
 */

/* Servlet package */
package com.inted.xcap.fe.error;


import org.w3c.dom.Document;
import org.w3c.dom.Element;

/*
 * CannotDelete base class
 */
public class CannotDelete extends ErrorCodes
{
  // --------------------------------------------------------------------------
  /*
   * Method: CannotDelete
   *         Constructor to initialize base class
   * @param  schemaLocation SchemaLocation for this error code
   * @param  reason descritive reason for this error code
   * <xs:element name="cannot-delete" substitutionGroup="error-element">
   *   <xs:annotation>
   *     <xs:documentation>
   *       This indicates that the requested DELETE operation could not be 
   *       performed because it would not be idempotent.
   *     </xs:documentation>
   *   </xs:annotation>
   *   <xs:complexType>
   *     <xs:attribute name="phrase" type="xs:string" use="optional"/>
   *   </xs:complexType>
   * </xs:element>
   *
   */
  public CannotDelete(String schemaLocation, String reason)
  {
    super(schemaLocation, reason);
    if(_error == false)
    {
      _cannotDelete = _document.createElement("cannot-delete"); 
      _cannotDelete.setAttribute("phrase", reason);
      _root.appendChild(_cannotDelete);
    }
  }; // CannotDelete


  //---------------------------------------------------------------------------
  /**
   * Method: setPhrase
   *         Method to set the phrase attribute of the error code
   * @param  phrase the String phrase to be set
   */
  public void setPhrase(String phrase)
  {
    _cannotDelete.setAttribute("phrase", phrase);
  }; // setPhrase


  /**
   * Method: getCannotDeleteElement
   *         Method to get the cannot-delete element of this error tree
   * @return Element specifies the cannot-delete element
   */
  public Element getCannotDeleteElement()
  { 
    return _cannotDelete;
  }; // getCannotDeleteElement


  /* Element for this error type */
  private Element _cannotDelete;

}; // class CannotDelete
