/*
 * "NotXMLAttValue.java (c) Fall 2005 Columbia University"
 */

/* Servlet package */
package com.inted.xcap.fe.error;


import org.w3c.dom.Document;
import org.w3c.dom.Element;

/*
 * NotXMLAttValue base class
 */
public class NotXMLAttValue extends ErrorCodes
{
  // --------------------------------------------------------------------------
  /*
   * Method: NotXMLAttValue
   *         Constructor to initialize base class
   * @param  schemaLocation SchemaLocation for this error code
   * @param  reason descritive reason for this error code
   * <xs:element name="not-xml-att-value" substitutionGroup="error-element">
   *   <xs:annotation>
   *     <xs:documentation>
   *       This indicates that the request was supposed to contain a valid 
   *       XML attribute value, but did not.
   *     </xs:documentation>
   *   </xs:annotation>
   *   <xs:complexType>
   *     <xs:attribute name="phrase" type="xs:string" use="optional"/>
   *   </xs:complexType>
   * </xs:element>
   */
  public NotXMLAttValue(String schemaLocation, String reason)
  {
    super(schemaLocation, reason);
    if(_error == false)
    {
      _notXMLAttValue = _document.createElement("not-xml-att-value"); 
      _notXMLAttValue.setAttribute("phrase", reason);
      _root.appendChild(_notXMLAttValue);
    }
  }; // NotXMLAttValue


  //---------------------------------------------------------------------------
  /**
   * Method: setPhrase
   *         Method to set the phrase attribute of the error code
   * @param  phrase the String phrase to be set
   */
  public void setPhrase(String phrase)
  {
    _notXMLAttValue.setAttribute("phrase", phrase);
  }; // setPhrase


  /**
   * Method: getNotXMLAttValueElement
   *         Method to get the not-xml-att-value element of this error tree
   * @return Element specifies the not-xml-att-value element
   */
  public Element getNotXMLAttValueElement()
  { 
    return _notXMLAttValue;
  }; // getNotXMLAttValueElement



  /* Element for this error type */
  private Element _notXMLAttValue;

}; // class NotXMLAttValue
