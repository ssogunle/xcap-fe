package com.inted.xcap.fe.error;
/*
 * "ErrorCodes.java (c) Fall 2005 Columbia University"
 */

/* Servlet package */

import java.io.StringWriter;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

//import org.apache.xml.serialize.OutputFormat;
//import org.apache.xml.serialize.XMLSerializer;

import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;
import javax.xml.XMLConstants;
import javax.xml.transform.Source;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Validator;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import java.io.StringReader;

import java.io.ByteArrayOutputStream;



/*
 * ErrorCodes base class
 */
public class ErrorCodes 
{
  /*
   * Method: ErrorCodes
   *         Constructor to initialize base class
   * @param  schemaLocation SchemaLocation for this error code
   * @param  reason descritive reason for this error code
   */
  public ErrorCodes(String schemaLocation, String reason) // ------------------
  {
    _reason = reason;
    _error  = false;
    _defNamespace = "urn:ietf:params:xml:ns:xcap-error";
//    _mimeType = SchemaConstants.ERROR_NS;

    try 
    {
      // Create Document
      DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
      dbf.setNamespaceAware(true);
      DocumentBuilder db         = dbf.newDocumentBuilder();
      _document                  = db.newDocument();
     
      // Create root element
      _root = _document.createElementNS(_defNamespace, "xcap-error");
      _document.appendChild(_root);
    }
    catch(Exception ex) 
    {
      ex.printStackTrace();
      _error = true;
    }
  }; // ErrorCodes


  /**
   * Method: serialize
   *         Method to get the xml string output of the xcap error format
   * @return String specifies the string output
   */
  public String serialize() // -------------------------------------------------
  {
    String result = null;
    if(_error == false)
    {
      try
      {
        TransformerFactory tfactory = TransformerFactory.newInstance(); 
        Transformer serializer = tfactory.newTransformer(); 
        Source source = new DOMSource(_document.getDocumentElement());
        ByteArrayOutputStream baout = new ByteArrayOutputStream();
        StreamResult streamResult = new StreamResult(baout);
        serializer.transform(source, streamResult);
        result = baout.toString();
      }
      catch(javax.xml.transform.TransformerConfigurationException tcException)
      {
        System.out.println("TException transforming: " + 
            tcException.getMessage());
      }
      catch(javax.xml.transform.TransformerException tException)
      {
        System.out.println("TException transforming: " + 
            tException.getMessage());
      }
    }
    else
    {
      result = _reason;
    }
    return result;
  }; // serialize


  /**
   * Method: getError
   *         Method to get any error than occured while processing the xcap
   *         error document
   * @return boolean spcifies the error
   */
  public boolean getError()
  {
    return _error;
  }; // getError


  /**
   * Method: getReason
   *         Method to get the extra rteason for failure
   * @return String specifies the reason
   */
  public String getReason()
  {
    return _reason;
  }; // getReason


  /**
   * Method: getRoot
   *         Method to get the root of the error xml doc
   * @return Element specifies the root of the error xml doc
   */
  public Element getRoot()
  {
    return _root;
  }; // getRoot


  /**
   * Method: getDocument
   *         Method to get the document of the error xml doc
   * @return Document specifies the document of the error xml doc         
   */
  public Document getDocument()
  {
    return _document;
  }; // getDocument


  /**
   * Method: getMimeType
   *         Method to get the mimetype of the error xml doc
   * @return String specifies the mimetype of the error xml doc         
   */
  public String getMimeType()
  {
    return _mimeType;
  }; // getMimeType

 
  /** DOMDocument for this error xml doc*/
  Document _document;

  /** Root of the error code*/
  Element _root;

  /** Reason for exception */
  String _reason;

  /** Communicate error */
  boolean _error;

  /* Default Namespace */
  String _defNamespace;

  /** Mime type for errors */
  String _mimeType;

}; // class ErrorCodes
