/**
 * "NotifyChangeMgr.java (c) Fall 2005 Columbia University"
 */

/** NotifyChangeMgr package */
package com.inted.xcap.fe.util;




/**
 * Class responsible for managing NotifyChange class instantiation
 */
public class NotifyChangeMgr
{

  /**
   * Method: NotifyChangeMgr
   *         Private constructor singleton pattern
   */
  private NotifyChangeMgr()
  {
  }; // NotifyChangeMgr
  
  
  /**
   * Method: getInstance
   *         Static method to get always one instance of this class
   * @return NotifyChangeMgr, singleton instance of this class        
   */
  public static NotifyChangeMgr getInstance()
    throws XCAPException
  {
    if(m_notifyChangeMgr == null)
    {
      m_notifyChangeMgr = new NotifyChangeMgr();
      m_notifyChange = new NotifyChange();
    }
    return m_notifyChangeMgr;
  }; // run

 
  /**
   * Method: getNotifyChange
   *         Method to get NotifyChange object 
   * @return NotifyChange, the NotifyChange object
   */
  public NotifyChange getNotifyChange()
  {
    return m_notifyChange;
  }; // getSocket

 
  /** Singelton NotifyChangeMgr object */
  private static NotifyChangeMgr m_notifyChangeMgr;

  /** NotifyChange object */
  private static NotifyChange m_notifyChange;

}; // NotifyChangeMgr
