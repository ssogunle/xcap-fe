/**
 * "ResultObject.java (c) Fall 2005 Columbia University"
 */

/** ResultObject package */
package com.inted.xcap.fe.util;

/** Class encapsulating the result of put operations */
public class ResultObject
{

  /**
   * Method: ResultObject
   *         Constructor
   * @param  result Result of the XCAP operation
   * @param  newCreation boolean specifying whether the XCAP operation
   *         resulted in creation of a new document. true mean a new document 
   *         was created.
   */
  public ResultObject(String result, boolean newCreation)
  {
    _result = result;
    _newCreation = newCreation;
  }

  
  /**
   * Method: getResult
   *         Method to get the result of the final PUT operation
   * @return String specifying the result of the final PUT operation
   */
  public String getResult()
  {
    return _result;
  }


  /**
   * Method: getNewCreation
   *         Method to get the newCreation value
   *         true means a new file was created.
   * @return boolean specifies whether the document was newly created
   */
  public boolean getNewCreation()
  {
    return _newCreation;
  }

  /** Result of the PUT operation */
  private String _result;

  /** New creation flag */
  private boolean _newCreation;
}
