/**
 * "HttpSemantics.java (c) Fall 2005 Columbia University"
 */

/** HttpSemantics package */
package com.inted.xcap.fe.util;

/** Enumeration for headers */
import java.util.Enumeration;

/** HTTP response codes */
import javax.servlet.http.HttpServletResponse;

import com.inted.xcap.fe.api.DocumentHandler;
import com.inted.xcap.fe.error.NullError;

import javax.servlet.http.HttpServletRequest;

import java.util.Enumeration;

/**
 * Class forms a base class for different store implementations
 */
public class HttpSemantics {

	/**
	 * Method: conditionalSemantics Method to check for HTTP specified E-Tag and
	 * conditional header semantics
	 * 
	 * @param HttpServletRequest
	 *            specifies the HTTP request
	 * @param HttpServletResponse
	 *            specifies the HTTP response
	 * @param DocumentHandler
	 *            specifies the document handler associated with this call
	 * @return boolean status of the operation., true means success
	 * @throws Exception
	 */
	public boolean conditionalSemantics(HttpServletRequest req, HttpServletResponse resp, DocumentHandler docHandler)
			throws Exception {
		// --------------------------------------------------------------------------
		System.out.println("**************Here");
		boolean status = false;
		String ifModSinceHeader = req.getHeader("If-Modified-Since");
		String ifUnmodifiedSinceHeader = req.getHeader("If-Unmodified-Since");
		Enumeration ifMatchHeader = req.getHeaders("If-Match");
		Enumeration ifNoneMatchHeader = req.getHeaders("If-None-Match");
		System.out.println("*************Here");

		boolean ifMatchPresent = ifMatchHeader.hasMoreElements();
		boolean ifNoneMatchPresent = ifNoneMatchHeader.hasMoreElements();
		boolean ifModSincePresent = false;
		boolean ifUnmodifiedSincePresent = false;

		if (ifModSinceHeader != null) {
			ifModSincePresent = true;
		}

		if (ifUnmodifiedSinceHeader != null) {
			ifUnmodifiedSincePresent = true;
		}
		System.out.println("*************Here");

		// if-Match && if-NoneMatch
		// if-match && if-ModSince
		if ((ifMatchPresent && ifNoneMatchPresent) || (ifMatchPresent && ifModSincePresent)) {
			throw new XCAPException(HttpServletResponse.SC_BAD_REQUEST,
					new NullError(SchemaConstants.NULLSCHEMA, "Syntax error"));
		}
		System.out.println("*************Here");

		// If-Modified-Since && If-Match
		// If-Modified-Since && If-Unmodified-Since
		if ((ifModSincePresent && ifMatchPresent) || (ifModSincePresent && ifUnmodifiedSincePresent)) {
			throw new XCAPException(HttpServletResponse.SC_BAD_REQUEST,
					new NullError(SchemaConstants.NULLSCHEMA, "Syntax error"));
		}
		System.out.println("*************Here");

		// If-None-Match && If-Unmodified-Since
		if (ifNoneMatchPresent && ifUnmodifiedSincePresent) {
			throw new XCAPException(HttpServletResponse.SC_BAD_REQUEST,
					new NullError(SchemaConstants.NULLSCHEMA, "Syntax error"));
		}
		System.out.println("*************Here");

		try {
			// String etag = docHandler.calcETag(AppUsage.BASE +
			// docHandler.getDocumentSelector());
			String etag = docHandler.calcETag(docHandler.getDocumentSelector());
			System.out.println("*******ETag: " + etag);
			resp.setHeader("ETag", "\"" + etag + "\"");

			if (ifMatchPresent) {
				System.out.println("New Etag calculated: " + etag);
				status = matchIfMatch(ifMatchHeader, etag);
				if (status == false) {
					throw new XCAPException(HttpServletResponse.SC_PRECONDITION_FAILED,
							new NullError(SchemaConstants.NULLSCHEMA, "Etag If-match dont match"));
				}
			} else if (ifNoneMatchPresent) {
				status = matchIfNoneMatch(ifNoneMatchHeader, etag);
				if (status == true) // MUST NOT perform request
				{
					if (ifModSincePresent) {
						if (ifModSinceHeader == docHandler.getModifyTime()) {
							throw new XCAPException(HttpServletResponse.SC_NOT_MODIFIED,
									new NullError(SchemaConstants.NULLSCHEMA, "Doc not modified"));
						}
					}
				} else {
					if (req.getMethod() == "GET") {
						resp.setHeader("ETag", "\"" + etag + "\"");
						throw new XCAPException(HttpServletResponse.SC_NOT_MODIFIED,
								new NullError(SchemaConstants.NULLSCHEMA, "Doc not modified"));
					} else if (req.getMethod() == "POST") {
						throw new XCAPException(HttpServletResponse.SC_PRECONDITION_FAILED,
								new NullError(SchemaConstants.NULLSCHEMA, "Doc not modified"));
					} else {
						throw new XCAPException(HttpServletResponse.SC_INTERNAL_SERVER_ERROR,
								new NullError(SchemaConstants.NULLSCHEMA, "Conditional header parsing wrong"));
					}
				}
			} else if (ifModSincePresent) {
				if (ifModSinceHeader == docHandler.getModifyTime()) {
					throw new XCAPException(HttpServletResponse.SC_NOT_MODIFIED,
							new NullError(SchemaConstants.NULLSCHEMA, "Doc not modified"));
				} else {
					status = true;
				}
			} else if (ifUnmodifiedSincePresent) {
				if (ifUnmodifiedSinceHeader.equals(docHandler.getModifyTime()) == false) {
					throw new XCAPException(HttpServletResponse.SC_PRECONDITION_FAILED,
							new NullError(SchemaConstants.NULLSCHEMA, "Doc not modified"));
				} else {
					status = true;
				}
			} else {
				status = false;
			}
			System.out.println("*********************************Here");
		} catch (XCAPException exception) {
			System.out.println("Exception in reading file");
			System.out.println("Exception.getCode" + exception.getCode());
			System.out.println("SC_NOT_FOUND" + HttpServletResponse.SC_NOT_FOUND);
			System.out.println("req.getContentType()" + req.getContentType());
			System.out.println("docHandler.getAppUsage().getMimeType()" + docHandler.getAppUsage().getMimeType());
			if (req.getContentType() != null) {
				if ((exception.getCode() == HttpServletResponse.SC_NOT_FOUND)
						&& (req.getContentType().equals(docHandler.getAppUsage().getMimeType()) == true)) {
					status = true;
				} else {
					throw exception;
				}
			} else {
				throw exception;
			}
		}

		return status;
	}; // conditionalSemantics

	/**
	 * Method: matchIfNoneMatch Method to match IfNoneMatch HTTP header
	 * 
	 * @param Enumeration
	 *            the IfNoneMatch header values
	 * @param etag
	 *            the string etag to be matched to
	 * @return boolean true if match
	 */
	boolean matchIfNoneMatch(Enumeration ifNoneMatch, String etag) {
		boolean if_none_match = false;

		while (ifNoneMatch.hasMoreElements()) {
			String v = (String) ifNoneMatch.nextElement();
			String n[] = v.split(",");
			for (int i = 0; i < n.length; ++i) {
				String value = n[i].trim();
				if (value.equals("\"*\"") || value.matches("(W/)?\"" + etag + "\"")) {
					if_none_match = true;
					etag = value;
					break;
				}
			}
		}

		return if_none_match;
	}; // matchIfNoneMatch

	/**
	 * Method: matchETag Method to check if the etag matches to If-Match and
	 * If-None-Match header for this document.
	 * 
	 * @param ifMatch
	 *            specifies the enumeration for If-Match header
	 * @param ifNoneMatch
	 *            specifies the enumeration for If-None-Match header
	 * @param etag
	 *            specifies the etag to be matched.
	 * @return boolean, specifies true if -> If-Match and If-None-Match are not
	 *         present -> If-Match matches the etag passed -> If-None-Match does
	 *         not match the etag passed. false otherwise
	 */
	boolean matchIfMatch(Enumeration ifMatch, String etag) {
		boolean if_match = false;

		while (ifMatch.hasMoreElements()) {
			String v = (String) ifMatch.nextElement();
			System.out.println("Request    etag: " + v);
			System.out.println("Calculated etag: " + "\"" + etag + "\"");
			String n[] = v.split(",");
			for (int i = 0; i < n.length; ++i) {
				String value = n[i].trim();
				System.out.println("Trimmed etag   : " + value);
				System.out.println("Calculated etag: " + "\"" + etag + "\"");
				// if(value.equals("\"*\"") || value.matches("(W/)?\"" + etag +
				// "\""))
				if (value.equals("\"*\"") || value.equals("\"" + etag + "\"")) {
					if_match = true;
					etag = value;
					break;
				}
			}
		}
		;
		System.out.println("if_match: " + if_match);
		return if_match;
	}; // matchETag

}; // class HttpSemantics
