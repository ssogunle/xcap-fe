/**
 * "NotifyChange.java (c) Fall 2005 Columbia University"
 */

/** NotifyChange package */
package com.inted.xcap.fe.util;

import java.lang.Thread;
import java.net.Socket;
import java.net.ServerSocket;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.inted.xcap.fe.error.NullError;

/**
 * Class responsible for accepting connections for different PAs
 */
public class NotifyChange extends Thread
{
Logger LOG = LoggerFactory.getLogger(NotifyChange.class);


/** My socket class wrapper to read data */
private MySocket m_mySocket;

/** java server socket to accept connections */
private ServerSocket m_serverSocket;

/** Error for this class */
boolean m_error;

  /**
   * Method: NotifyChange
   *         Constructor to construct the server side socket
   * @throws XCAPException        
   */
  public NotifyChange()
    throws XCAPException
  {
    System.out.println("-->NotifyChange::NotifyChange");
    try
    {
      m_serverSocket = new ServerSocket(SocketConstants.PORT);
      setError(false);
      System.out.println("Created new server side socket");
    }
    catch(java.io.IOException exception)
    {
      System.out.println("Exception Creating new server side socket");
      setError(true);
      throw new XCAPException(HttpServletResponse.SC_INTERNAL_SERVER_ERROR,
          new NullError(SchemaConstants.NULLSCHEMA, exception.getMessage()));
    }
    System.out.println("NotifyChange::NotifyChange-->");
  }; // NotifyChange
  
  
  /**
   * Method: run
   *         Thread implementation. Methods accepts tcp connections and
   *         stores the ip and port. Only the latest connection is stored, so 
   *         everytime a  client connects the previous connection is closed
   */
  public void run()
  {
    LOG.info("-->NotifyChange::run");
    while(true)
    {
    	LOG.info("in while");
      try
      {
        LOG.info("Trying to create a socket:accept");
        Socket socket = m_serverSocket.accept();
        LOG.info("Accepted connection");
        
        if(m_mySocket != null)
        {
          try
          {
            m_mySocket.close();
          }
          catch(java.lang.Exception exception)
          {
          }
        }
        m_mySocket = new MySocket(socket);
        System.out.println("Created new m_mySocket");
      }
      catch(java.io.IOException exception)
      {
        System.out.println("Error creating  a socket");
        setError(true);
      }
      System.out.println("repeating while");
    }
  }; // run

 
  /**
   * Method: accept
   *         Methods accepts tcp connections and store the ip and port. 
   *         Only the latest connection is stored, so everytime a client 
   *         connects the previous connection is closed
   * @throws XCAPException        
   */
  public void accept()
    throws XCAPException
  {
    System.out.println("-->NotifyChange::accept");
    try
    {
      System.out.println("Trying to create a socket:accept");
      Socket socket = m_serverSocket.accept();
      System.out.println("Accepted connection");
      if(m_mySocket != null)
      {
        try
        {
          m_mySocket.close();
        }
        catch(java.lang.Exception exception)
        {
          throw new XCAPException(HttpServletResponse.SC_INTERNAL_SERVER_ERROR,
           new NullError(SchemaConstants.NULLSCHEMA, "Socket not initialized"));
        }
      }
      m_mySocket = new MySocket(socket);
      System.out.println("Created new m_mySocket");
    }
    catch(java.io.IOException exception)
    {
      throw new XCAPException(HttpServletResponse.SC_INTERNAL_SERVER_ERROR,
       new NullError(SchemaConstants.NULLSCHEMA, "Socket not initialized"));
    }
  }; // run


  /**
   * Method: getSocket
   *         Method to get socket for reading and writing
   * @return MySocket socket to read/write
   * @throws XCAPException
   */
  public MySocket getSocket()
    throws XCAPException
  {
    if(m_mySocket == null)
    {
      throw new XCAPException(HttpServletResponse.SC_INTERNAL_SERVER_ERROR,
          new NullError(SchemaConstants.NULLSCHEMA, "Socket not initialized"));
    }
    return m_mySocket;
  }; // getSocket

 
  /**
   * Method: setError
   *         Method to set/unset the error occured with accpeting
   *         connections
   * @param  error boolean value, true indicates error occured, false
   *         otherwise   
   */
  public void setError(boolean error)
  {
    m_error = error;
  }; // setError


  /**
   * Method: getError
   *         Method to get the error. Error is set to true if any operation
   *         by this class created a un-throwable exception eg. exception in
   *         a thread.
   * @return boolean true indicates error occured, false otherwise
   */
  public boolean getError()
  {
    return m_error;
  }; // getError
 
} // NotifyChange
