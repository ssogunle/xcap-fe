/**
 * "SocketConstants.java (c) Fall 2005 Columbia University"
 */

/** SocketConstants package */
package com.inted.xcap.fe.util;

/**
 * Constants class for storing the pre-defined schema files
 */
public class SocketConstants
{
  public static final int PORT = 50061;
}; // class SocketConstants
