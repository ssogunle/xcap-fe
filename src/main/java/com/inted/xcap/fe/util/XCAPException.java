package com.inted.xcap.fe.util;

/** Servlet package */

/** HttpServlet imports */
import java.lang.Exception;

import com.inted.xcap.fe.error.ErrorCodes;



/**
 * XCAPException class for propogating xcap conflicts along with the reason for
 * their occurance.
 */
public class XCAPException extends Exception {
	
	/** ErrorCode of exception */
	private int code;

	/** Reason for exception */
	private ErrorCodes errorCode;

	
	/**
	 * Method: XCAPException Constructor to initialize base class
	 * 
	 * @param code
	 *            int specifying the HTTP error code
	 * @param errorCode
	 *            ErrorCodes specifying the detailed error code in form of XCAP
	 *            conflict XML docs
	 */
	public XCAPException(int code, ErrorCodes errorCode) {
		this.code = code;
		this.errorCode = errorCode;
		printStackTrace();
	}; // XCAPException

	/**
	 * Method: getErrorCode Method to get the error code for the exception
	 * 
	 * @return ErrorCodes specifies the error code xcap conflict.
	 */
	public ErrorCodes getErrorCode() {
		return errorCode;
	}; // getErrorCode

	/**
	 * Method: getCode Method to get the error code for the exception
	 * 
	 * @return int, specifies the error code
	 */
	public int getCode() {
		return code;
	}; // getErrorCode

	/**
	 * Method: getReason Method to get the reason for the exception
	 * 
	 * @return string specifies the reason.
	 */
	public String getReason() {
		return errorCode.getReason();
	}; // getReason

	
}; // class XCAPException
