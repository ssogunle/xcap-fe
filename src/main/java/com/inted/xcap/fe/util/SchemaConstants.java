/**
 * "SchemaConstants.java (c) Fall 2005 Columbia University"
 */

/** SchemaConstants package */
package com.inted.xcap.fe.util;

/**
 * Constants class for storing the pre-defined schema files
 */
public class SchemaConstants {
	public static final String W3C_SCHEMALOC = "http://www.w3.org/2001/XMLSchema";

	public static final String NULLSCHEMA = null;

	public static final String DOCUMENT = "document";
	public static final String ELEMENT = "element";
	public static final String ATTRIBUTE = "attribute";
	public static final String WHOLE = "whole";

	public static final String NAMESPACE_SEL = "namespace::*";

	public static final String ELEMENT_NS = "\"application/xcap-el+xml\"";
	public static final String ATT_NS = "\"application/xcap-att+xml\"";
	public static final String ERROR_NS = "\"application/xcap-error+xml\"";
	public static final String NAMESPACE_NS = "\"application/xcap-ns+xml\"";

	public static final String CLASS_LOADER = "applocator.txt";

	public static final String UTF_8 = "UTF-8";

	public static final int NAMESPACES_LEN = 100;

}; // class SchemaConstants
