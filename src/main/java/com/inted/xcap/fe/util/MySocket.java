/**
 * "MySocket.java (c) Fall 2005 Columbia University"
 */

/** MySocket package */
package com.inted.xcap.fe.util;

import java.net.Socket;
import java.io.OutputStream;

import java.io.File;
import java.io.DataInputStream;
import java.io.FileReader;
import java.io.StringWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.inted.xcap.fe.error.NullError;

/**
 * Class responsible for reading and writing to sockets created by
 * edu.columbia.xcap.utils.NotifyChange class
 */
public class MySocket
{

  /**
   * Method: MySocket
   *         Constructor to construct the socket
   * @param  Socket specifying the socket to be used to send and recv        
   */
  public MySocket(Socket socket)
  {
    m_socket = socket;
  }; // MySocket
  

  /**
   * Method: read
   *         Method to read from the socket
   * @return String String rea
   * @throws XCAPException
   */
  public String read()
    throws XCAPException
  {
    String result = "";
    DataInputStream input;
    try 
    {
      input = new DataInputStream(m_socket.getInputStream());
      byte[] readByte = new byte[100];
      input.read(readByte);
      String readStr = new String(readByte, 0, 6);
      System.out.println("readStr:" + readStr);

      File dir = new File("/proj/irt-filer2/anurag/xcap/testAU/services/pres-rules/users");
      String[] children = dir.list();

      for (int i=0; i<children.length; i++) 
      {
        StringWriter out = new StringWriter();
        String filename = children[i];
        filename = "/proj/irt-filer2/anurag/xcap/testAU/services/pres-rules/users/" + filename;
        System.out.println("filename: " + filename);
        File moredir = new File(filename);
        if(moredir.isDirectory() == false)
        {
          System.out.println("Not a directory");
        }
        else
        {
          System.out.println("asd");
          String[] morechildren = moredir.list();
          for (int j=0; j<morechildren.length; j++)
          {
            String realfilename = filename + "/" + morechildren[j];
            System.out.println("realfilename" + realfilename);
            File file = new File(realfilename);
            FileReader in = new FileReader(file);
            int c;
            while((c = in.read()) != -1)
            {
              out.write(c);
            }
            String interresult = out.toString();
            interresult = interresult + "\n";
            write(interresult);
          }
        }
      }
      System.out.println("output:" + result);
    }
    catch(java.io.IOException exception) 
    {
      exception.printStackTrace();
      throw new XCAPException(HttpServletResponse.SC_INTERNAL_SERVER_ERROR,
        new NullError(SchemaConstants.NULLSCHEMA, exception.getMessage()));
    }
    return result;
  }; // read


  /**
   * Method: write
   *         Method to write into the socket
   * @param  data String to write
   * @throws XCAPException
   */
  public void write(String data)
    throws XCAPException
  {
    try
    {
      OutputStream os = m_socket.getOutputStream();
      os.write(data.getBytes());
      os.flush();
    }
    catch(java.io.IOException exception)
    {
      throw new XCAPException(HttpServletResponse.SC_INTERNAL_SERVER_ERROR,
          new NullError(SchemaConstants.NULLSCHEMA, exception.getMessage()));
    }
  }; // run

  /**
   * Method: close
   *         Method to close the socket
   * @throws XCAPException        
   */
  public void close()
    throws XCAPException
  {
    try
    {
      m_socket.close();
    }
    catch(java.io.IOException exception)
    {
      throw new XCAPException(HttpServletResponse.SC_INTERNAL_SERVER_ERROR,
          new NullError(SchemaConstants.NULLSCHEMA, exception.getMessage()));
    }
  }; // close


  /** My socket class wrapper to read data */
  private Socket m_socket;

}; // MySocket
