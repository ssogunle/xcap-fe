package com.inted.xcap.fe;

import java.net.URI;
import java.util.List;

import org.apache.olingo.client.api.ODataClient;
import org.apache.olingo.client.api.communication.request.cud.CUDRequestFactory;
import org.apache.olingo.client.api.communication.request.cud.ODataEntityCreateRequest;
import org.apache.olingo.client.api.communication.request.retrieve.ODataEntitySetRequest;
import org.apache.olingo.client.api.communication.request.retrieve.RetrieveRequestFactory;
import org.apache.olingo.client.api.communication.response.ODataResponse;
import org.apache.olingo.client.api.communication.response.ODataRetrieveResponse;
import org.apache.olingo.client.api.domain.ClientEntity;
import org.apache.olingo.client.api.domain.ClientEntitySet;
import org.apache.olingo.client.api.domain.ClientObjectFactory;
import org.apache.olingo.client.core.ODataClientFactory;
import org.apache.olingo.commons.api.edm.FullQualifiedName;

public class CsdrClient {
	// ODATA EDM Namespace
		private static final String NAMESPACE = "com.inted.csdr";

		private static final String ES_DOC_NAME = "XcapDocuments";

		private static final String ET_DOC_NAME = "XcapDocument";

		
		// OData Service URL
		private final String serviceRoot = "http://localhost:8080/csdr/svc/";

		// Create OData V4 Client
		private ODataClient client;

		private ClientObjectFactory factory;

		public CsdrClient() {
			client = ODataClientFactory.getClient();
			factory = client.getObjectFactory();
		}
		
		public int addDocument(XcapDocument doc){
			
			URI entitySetUri = client.newURIBuilder(serviceRoot).appendEntitySetSegment(ES_DOC_NAME).build();
			String fqn = NAMESPACE + "." + ET_DOC_NAME;
			FullQualifiedName cdrFqn = new FullQualifiedName(fqn);
			ClientEntity et = client.getObjectFactory().newEntity(cdrFqn);

			et.getProperties().add(factory.newPrimitiveProperty("AppUsage",
					factory.newPrimitiveValueBuilder().buildString(doc.getAppUsage())));
			
			et.getProperties().add(factory.newPrimitiveProperty("Data",
					factory.newPrimitiveValueBuilder().buildString(doc.getData())));
			
			et.getProperties().add(factory.newPrimitiveProperty("eTag",
					factory.newPrimitiveValueBuilder().buildString(doc.geteTag())));
			
			et.getProperties().add(factory.newPrimitiveProperty("DocumentName",
					factory.newPrimitiveValueBuilder().buildString(doc.getName())));
			
			et.getProperties().add(factory.newPrimitiveProperty("Xui",
					factory.newPrimitiveValueBuilder().buildString(doc.getXui())));
			
			et.getProperties().add(factory.newPrimitiveProperty("IsUpdated",
					factory.newPrimitiveValueBuilder().buildBoolean(doc.getIsUpdated())));
			
			int resultCode = createEntity(entitySetUri, et);

			return resultCode;
		}
		
		public  ClientEntity fetchDocument(String appUsage, String xui, String docName){
			
			URI entitySetUri = client.newURIBuilder(serviceRoot).appendEntitySetSegment(ES_DOC_NAME)
					.filter("AppUsage eq '" + appUsage + "' and Xui eq '" + xui + "'" +
			"' and DocumentName eq '" + docName + "'").build();
			List<ClientEntity> set = fetchEntities(entitySetUri);
			
			if (set.size() == 1)
				return set.get(0);

			return null;
		}
		
		
		public int createEntity(URI targetUri, ClientEntity entity) {

			CUDRequestFactory crf = client.getCUDRequestFactory();

			ODataEntityCreateRequest request = crf.getEntityCreateRequest(targetUri, entity);
			request.setAccept("application/json;odata.metadata=minimal");

			ODataResponse response = request.execute();

			return response.getStatusCode();
		}

		public List<ClientEntity> fetchEntities(URI entitySetUri) {

			RetrieveRequestFactory rrf = client.getRetrieveRequestFactory();

			ODataEntitySetRequest<ClientEntitySet> request = rrf.getEntitySetRequest(entitySetUri);
			request.setAccept("application/json;odata.metadata=minimal");

			ODataRetrieveResponse<ClientEntitySet> response = request.execute();

			ClientEntitySet retrievedEntitySet = response.getBody();

			return retrievedEntitySet.getEntities();
		}
}
