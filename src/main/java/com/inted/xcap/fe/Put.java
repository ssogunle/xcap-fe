/**
 * "Put.java (c) Fall 2005 Columbia University"
 */

/** Put package */
package com.inted.xcap.fe;

import java.util.List;


/** jaxp imports */
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.transform.Source;
import javax.xml.validation.Validator;

import java.io.ByteArrayInputStream;

/** dom node, namespace imports */
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Element;
import org.w3c.dom.Document;
import org.w3c.dom.ls.LSSerializer;
import org.w3c.dom.ls.DOMImplementationLS;
import org.w3c.dom.bootstrap.DOMImplementationRegistry;
import org.w3c.dom.DOMException;

import org.xml.sax.InputSource;

/** HTTP response codes */
import javax.servlet.http.HttpServletResponse;

import com.inted.xcap.fe.parsers.DOMXMLParser;
/** Parser import */
import com.inted.xcap.fe.parsers.SaxonParser;

/** Application Usage */
import com.inted.xcap.fe.AppUsages.AppUsage;
import com.inted.xcap.fe.AppUsages.AppUsageFactory;
import com.inted.xcap.fe.error.CannotInsert;
import com.inted.xcap.fe.error.NoParent;
import com.inted.xcap.fe.error.NullError;
/** Exceptions */
import com.inted.xcap.fe.util.XCAPException;

/** java file */
import java.io.StringReader;
import java.io.FileInputStream;

import com.inted.xcap.fe.parsers.SchemaValidator;
import com.inted.xcap.fe.util.ResultObject;
import com.inted.xcap.fe.util.SchemaConstants;

import net.sf.saxon.om.NodeInfo;

import java.lang.*;
import java.util.*;

/**
 * Class encapsulating XCAP PUT specification implementation
 */
public class Put
{

  /**
   * Method: putEntry
   *         Method to put the given document position pointed by the 
   *         XCAP expressio
   * @param  nodeSelector specifies the XCAP nodeSelector
   * @param  lastPathSegment specifies the last path segment of the XCAP
   *         URI 
   * @param  query specifies the XCAP expression where the document has
   *         to be inserted.
   * @param  serverBody specifies the document into which insert happens
   * @param  reqBody specifies the body in the PUT request
   * @param  appUsage specifies the application uage for which the PUT
   *         operation has come.
   * @param  mimeType specifies the mime-type specified in the PUT
   *         operation
   * @return ResultObject specifying the result of the PUT operation
   * @throws XCAPException        
   */
  public ResultObject putEntry(String nodeSelector, String lastPathSegment, 
      String query, Object serverBody, String reqBody, AppUsage appUsage,
      String mimeType)
    throws XCAPException
  {
    boolean newCreation = false;
    // NodeList nodeList = buildDOMTree(complete, inPut);
    
    // ------------------------------------------------------------------------
    System.out.println("nodeSelector   : " + nodeSelector);
    System.out.println("lastPathSegment: " + lastPathSegment);
    System.out.println("query          : " + query);
    System.out.println("reqBody        : " + reqBody);

    if(lastPathSegment == SchemaConstants.NAMESPACE_SEL)
    {
      throw new XCAPException(HttpServletResponse.SC_BAD_REQUEST,
          new NullError(SchemaConstants.NULLSCHEMA,
            "Namespace selector not allowed in PUT request"));
    }

    Document inPut = null;
    NodeList parentContext = null;

    String schemaType = new String(SchemaConstants.WHOLE);
    if(mimeType.equals(SchemaConstants.ATT_NS) == true)
    {
      schemaType = SchemaConstants.ATTRIBUTE;
    }
    else if(mimeType.equals(SchemaConstants.ELEMENT_NS) == true)
    {
      schemaType = SchemaConstants.ELEMENT;
    }
    else
    {
      throw new XCAPException(HttpServletResponse.SC_UNSUPPORTED_MEDIA_TYPE, 
          new NullError(SchemaConstants.NULLSCHEMA, "Wrong media type"));
    }
    
    DOMXMLParser xmlParser = new DOMXMLParser(true);
    Document complete = 
      (Document)xmlParser.parse(((FileInputStream)serverBody));
    SaxonParser.print(complete, "complete");
    if(mimeType.equals(SchemaConstants.ATT_NS) == false)
    {
      System.out.println("Using namespace aware parser");
      System.out.println("reqBody: " + reqBody);
      inPut = (Document)xmlParser.parse( 
          new ByteArrayInputStream(reqBody.getBytes()));
    }
    SaxonParser parser = new SaxonParser();
    System.out.println("nodeSelector: " + nodeSelector);
    parentContext = parser.evaluateXPATH(nodeSelector, query, complete, 
        appUsage);

    // ------------------------------------------------------------------------
    String result = null;
    
    NodeList nodeList = null;
    System.out.println("parentContext.getLength: " + parentContext.getLength());
    if(parentContext.getLength() == 1)
    {
      SaxonParser.print(parentContext.item(0), "ParentContext");
      // parent/*[position][unique-attribute-value]
      nodeList = parser.evaluateXPATH(lastPathSegment, query, 
          parentContext.item(0), appUsage);
      System.out.println("lastPathSegment nodelist: " + nodeList.getLength());
      if(nodeList.getLength() == 1)
      {
        SaxonParser.print(nodeList.item(0), "lastPathSegment");
        System.out.println("nodeList.item(0) to be updated by inPut");
        // nodeList.item(0) to be updated by inPut
        newCreation = updateNode(complete, nodeList.item(0), inPut, reqBody, 
            appUsage, "update", mimeType);
      }
      else if(nodeList.getLength() == 0)
      {
        System.out.println("**********lastPathSegment did not return anything");
        if(lastPathSegment.matches(".*\\[\\d+\\]\\[.*\\]"))
        {
          System.out.println("Hey Double Square Bracket");
          int lastSquareBracket = lastPathSegment.lastIndexOf("[");
          String modLastPath = lastPathSegment.substring(0, lastSquareBracket);
          System.out.println("modLastPath: " + modLastPath);
          nodeList = parser.evaluateXPATH(modLastPath, query, 
              parentContext.item(0), appUsage);
          if(nodeList.getLength() == 1)
          {
            System.out.println("inPut to be inserted before nodeList.item(0)");
            // inPut to be inserted before nodeList.item(0)
            newCreation = updateNode(complete, nodeList.item(0), inPut, 
                reqBody, appUsage, "insert-before", mimeType);
          }
          else if(nodeList.getLength() == 0)
          {
            System.out.println("Hey Double Square Bracket Append");
            int secondLastSquareBracketBegin = modLastPath.lastIndexOf("[");
            int secondLastSquareBracketEnd = modLastPath.lastIndexOf("]");
            String posToInsertStr = lastPathSegment.substring(
                secondLastSquareBracketBegin+1, secondLastSquareBracketEnd);
            System.out.println("Got integer: " + posToInsertStr);
            Integer posToInsertInt = new Integer(posToInsertStr);
            int posToInsert = posToInsertInt.intValue();
            System.out.println("Integer value: " + posToInsert);
            
            int previousPos = posToInsert - 1;
            System.out.println("Previous position: " + previousPos);

            String newPath = modLastPath.substring(0, 
                secondLastSquareBracketBegin+1); 
            newPath = newPath + previousPos;
            newPath = newPath + "]";
            System.out.println("new Path: " + newPath);
            nodeList = parser.evaluateXPATH(newPath, query, 
                parentContext.item(0), appUsage);
            if(nodeList.getLength() != 1)
            {
              System.out.println("Check out the index value");
              throw new XCAPException(HttpServletResponse.SC_NOT_FOUND, 
                  new NullError(SchemaConstants.NULLSCHEMA, 
                    "Invalid/No-Match"));
            }
            else
            {
              // inPut to be inserted after nodeList.item(0)
              newCreation = updateNode(complete, nodeList.item(0), inPut, 
                  reqBody, appUsage, "insert-after", mimeType);
            }
          }
        }
        else if(lastPathSegment.matches(".*\\[\\d+\\]"))
        {
          System.out.println("Hey dood Single match");
          int lastSquareBracketBegin = lastPathSegment.lastIndexOf("[");
          int lastSquareBracketEnd = lastPathSegment.lastIndexOf("]");
          String posToInsertStr = lastPathSegment.substring(
              lastSquareBracketBegin+1, lastSquareBracketEnd);
          System.out.println("posToInsertStr: " + posToInsertStr);
          Integer posToInsertInt = new Integer(posToInsertStr);
          int posToInsert = posToInsertInt.intValue();
          System.out.println("posToInsert: " + posToInsert);

          int previousPos = posToInsert - 1;
          System.out.println("previousPos: " + previousPos);

          String newPath = lastPathSegment.substring(0,
              lastSquareBracketBegin+1);
          newPath = newPath + previousPos;
          newPath = newPath + "]";
          System.out.println("newPath: " + newPath);
          nodeList = parser.evaluateXPATH(newPath, query, parentContext.item(0),
              appUsage);
          if(nodeList.getLength() != 1)
          {
            System.out.println("No match");
            throw new XCAPException(HttpServletResponse.SC_NOT_FOUND, 
                new NullError(SchemaConstants.NULLSCHEMA, "Invalid/No-Match"));
          }
          else
          {
            System.out.println("Insert after");
            // inPut to be inserted after nodeList.item(0)
            newCreation = updateNode(complete, nodeList.item(0), inPut, 
                reqBody, appUsage, "insert-after", mimeType);
          }
        }
        else if(lastPathSegment.matches(".*\\[.*\\]"))
        {
          System.out.println("Last one");
          newCreation = updateNode(complete, parentContext.item(0), inPut, 
              reqBody, appUsage, "insert-within", mimeType);
        }
        else
        {
          newCreation = updateNode(complete, parentContext.item(0), inPut, 
              reqBody, appUsage, "insert-within", mimeType);
        }
      }
      else
      {
        throw new XCAPException(HttpServletResponse.SC_CONFLICT, 
            new NullError(SchemaConstants.NULLSCHEMA, "No-Match/Invalid"));
      }
    }
    else
    {
      throw new XCAPException(HttpServletResponse.SC_CONFLICT, 
          new NoParent(SchemaConstants.NULLSCHEMA, "No-Match/Invalid"));
    }
  
    SaxonParser.print(complete, "Complete after replacement");
    SchemaValidator schemaValidator = new 
      SchemaValidator(SchemaConstants.WHOLE);
    schemaValidator.validateSchema(appUsage.getSchemaLocation(), complete);

    NodeList testNodeList = null;
    if((lastPathSegment.equals("") == false) && (lastPathSegment != null))
    {
      testNodeList = parser.evaluateXPATH(nodeSelector+ "/" + lastPathSegment, 
        query, complete, appUsage);
      System.out.println("complete path selector: " + 
          nodeSelector+"/"+lastPathSegment);
    }
    else
    {
      testNodeList = parser.evaluateXPATH(nodeSelector, query, complete, 
          appUsage);
    }

    System.out.println("No. of lists: " + testNodeList.getLength());
    if(testNodeList.getLength() != 1)
    {
      throw new XCAPException(HttpServletResponse.SC_CONFLICT,
          new CannotInsert(SchemaConstants.NULLSCHEMA, "cannot insert"));
    }
    else
    {
      if(mimeType.equals(SchemaConstants.ATT_NS) == false)
      {
        if(testNodeList.item(0).isEqualNode((Node)inPut.getFirstChild()) == 
            true)
        {
          System.out.println("Working:-)");
          result = SaxonParser.print(complete, "Complete after replacement");
        }
        else
        {
          throw new XCAPException(HttpServletResponse.SC_CONFLICT,
              new CannotInsert(SchemaConstants.NULLSCHEMA, "Cannot insert"));
        }
      }
      else
      {
        System.out.println("Working:-)");
        result = SaxonParser.print(complete, "Complete after replacement");
      }
    }
    appUsage.ensureUniqueness();
    System.out.println("Result: " + result);
    System.out.println("newCreation: " + newCreation);
    ResultObject resultObject = new ResultObject(result, newCreation);
    return(resultObject);
  }; // putEntry

  // ---------------------------------------------------------------------------
  /**
   * Method: updateNode
   *         Method to update the give node with the new element/attribute
   *         in the PUt operations
   * @param  complete DOM Document of the string to be updated
   * @param  nodeToBeUpdated DOM Node specifying the node to be updated
   * @param  nodeToBePut DOM Node specifying the node to be inserted
   * @param  reqBody specifying the request body in the PUT operation.
   * @param  appUsage specifying the AppUsage for the put operation
   * @param  action string specifying whethere to update or insert
   * @param  mimeType specifies the mime-type specified in the PUT
   *         operation
   * @return boolean specifying the result of the updateNode aoperation
   * @throws XCAPException        
   */
  private boolean updateNode
  (
    Document complete, 
    Node nodeToBeUpdated,
    Document nodeToBePut, 
    String reqBody, 
    AppUsage appUsage, 
    String action,
    String mimeType
  )
  throws XCAPException
  {
    boolean newCreation = false;
    System.out.println("-->Put::updateNode");
    System.out.println("action: " + action);
    SaxonParser.print(nodeToBeUpdated, "node to be updated");
    /** XCAP URI returns a node, so if not positional then replace */
    //if(appUsage.getMimeType().equals(SchemaConstants.ELEMENT_NS))
    if(mimeType.equals(SchemaConstants.ELEMENT_NS))
    {

      Node adoptInPut = null;
      try
      {
        adoptInPut = complete.importNode(nodeToBePut.getFirstChild(), true);
        SaxonParser.print(adoptInPut, "adoptInPut");
      }
      catch(org.w3c.dom.DOMException exception)
      {
        System.out.println("Exception: " + exception.getMessage());
        throw new XCAPException(HttpServletResponse.SC_INTERNAL_SERVER_ERROR,
            new NullError(SchemaConstants.NULLSCHEMA, "Could not adopt node"));
      }

      boolean flag = false;
      Element parent = null;
      if(action.equals("insert-within") == true)
      {
        parent = (Element)nodeToBeUpdated;
        flag = true;
      }
      else if(action.equals("insert-after") == true)
      {
        System.out.println("**insert-after");
        Node nextSibling = nodeToBeUpdated.getNextSibling();
        if(nextSibling != null)
        {
          System.out.println("not null");
          while(nextSibling.getNodeType() != Node.ELEMENT_NODE)
          {
            System.out.println("not an element");
            nextSibling = nextSibling.getNextSibling();
            System.out.println("next nextSibling");
            if(nextSibling == null)
            {
              System.out.println("Next sibling null");
              parent = append(nodeToBeUpdated);
              flag = true;
              break;
            }
          }
          if(flag == false)
          {
            nodeToBeUpdated = (Element)nextSibling;

            action = "insert-before";
            parent = append(nodeToBeUpdated);
          }
        }
        else
        {
          parent = append(nodeToBeUpdated);
        }
      }
      else
      {
        parent = append(nodeToBeUpdated);
      }

      SaxonParser.print(parent, "********Parent**********");

      if(action.equals("update") == true)
      {
        SaxonParser.print(nodeToBeUpdated, "Node to be update");
        System.out.println("About to update complete");
        SaxonParser.print(adoptInPut, "adoptInPut");
        parent.replaceChild(adoptInPut, nodeToBeUpdated);
        System.out.println("Updated complete");
      }
      else if(action.equals("insert-before") == true)
      {
        System.out.println("insert-before");
        parent.insertBefore(adoptInPut, nodeToBeUpdated);
        newCreation = true;
      }
      else if(action.equals("insert-after") == true)
      {
        System.out.println("++insert-after");
        parent.appendChild(adoptInPut);
        newCreation = true;
      }
      else if(action.equals("insert-within") == true)
      {
        nodeToBeUpdated.appendChild(adoptInPut);
        newCreation = true;
      }
      else
      {
        throw new XCAPException(HttpServletResponse.SC_INTERNAL_SERVER_ERROR,
            new NullError(SchemaConstants.NULLSCHEMA, "Action not supported"));
      }
    }
    else if(mimeType.equals(SchemaConstants.ATT_NS))
    {
      try
      {
        String att[] = reqBody.split(":");
        System.out.println("Attribute name : " + att[0]);
        System.out.println("Attribute value: " + att[1]);
        Element elementToChange = (Element)nodeToBeUpdated;
        if((action.equals("update") == true) || 
          (action.equals("insert-before") == true))
        {
          if(elementToChange.hasAttribute(att[0]))
          {
            newCreation = true;
          }
          elementToChange.setAttribute(att[0], att[1]);
        }
        else if(action.equals("insert-after") == true)
        {
          throw new XCAPException(HttpServletResponse.SC_BAD_REQUEST,
              new NullError(SchemaConstants.NULLSCHEMA,
                "Action not supported with the given mime type"));
        }
      }
      catch(DOMException exception)
      {
        throw new XCAPException(HttpServletResponse.SC_BAD_REQUEST,
            new NullError(SchemaConstants.NULLSCHEMA, "INVALID_CHARACTER_ERR"));
      }
    }
    else
    {
      throw new XCAPException( HttpServletResponse.SC_UNSUPPORTED_MEDIA_TYPE, 
          new NullError(SchemaConstants.NULLSCHEMA, "Check the media specs"));
    }
    System.out.println("Put::updateNode-->");
    return newCreation;
  }; // updateNode

  /**
   * Method: append
   *         Method to form a new element of the node to be updated element
   * @param  nodeToBeUpdated specifies the node to be updated        
   * @return Element DOM Element        
   * @throws XCAPException        
   */
  Element append(Node nodeToBeUpdated)
    throws XCAPException
  {
    Element parent = null;
    Node parentNode = nodeToBeUpdated.getParentNode();
    while(parentNode.getNodeType() != Node.ELEMENT_NODE)
    {
      parentNode = parentNode.getParentNode();
      if(parentNode == null)
      {
        throw new XCAPException(HttpServletResponse.SC_CONFLICT,
            new NoParent(SchemaConstants.NULLSCHEMA, 
              "Xpath expression not supported"));
      }
    }
    parent = (Element)parentNode;
    return parent;
  }
    
}; // class Put
