package com.inted.xcap.fe.AppUsages;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AppUsageFactory {

	Logger LOG = LoggerFactory.getLogger(AppUsageFactory.class);

	Map<String, AppUsage> appUsageSet = new HashMap<String, AppUsage>();

	public static final AppUsageFactory INSTANCE = new AppUsageFactory();
	
	public AppUsageFactory() {

	}

	public void registerApplicationUsage(AppUsage appUsage) {
		appUsageSet.put(appUsage.getAuid(), appUsage);
	}

	public AppUsage getApplicationUsage(String auid) throws Exception {

		AppUsage appUsage = (AppUsage) appUsageSet.get(auid);

		LOG.info("appUsage:" + auid);

		if (appUsage == null)
			throw new Exception("AppUsage not found!");

		return appUsage;
	}
}
