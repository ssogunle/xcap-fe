package com.inted.xcap.fe.AppUsages;

/*
 * Class for ResourceList Application
 */
public class ResourceListApp extends AppUsage {

	public ResourceListApp() {
		super("resource-lists", "def", "urn:ietf:params:xml:ns:resource-lists", "rlschema.xsd", "resource-lists",
				"\"" + "application/resource-lists+xml" + "\"");
	}
}