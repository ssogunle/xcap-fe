package com.inted.xcap.fe.AppUsages;

import com.inted.xcap.fe.util.XCAPException;

/**
 * 
 * @author Segun Sogunle
 *
 *Base AppUsage Class
 */

public class AppUsage {

	protected String appName;
	protected String auid;
	protected String defaultNamespaceName;
	protected String defaultNamespaceValue;
	protected String schemaLocation;
	protected String mimeType;
	//public static final String BASE = "/n/irt-filer1/proj/irt-filer2/anurag/xcap/testAU/";

	
	public AppUsage (String appName, String auid, String defaultNamespaceName, String defaultNamespaceValue, String schemaLocation,
			 String mimeType){
		
	  this.appName= appName;
	  this.auid = auid;
	  this.defaultNamespaceName = defaultNamespaceName;
	  this.defaultNamespaceValue = defaultNamespaceValue;
	  this.schemaLocation = schemaLocation;
	  this.mimeType = mimeType;
	  
	}

	public String getAppName() {
		return appName;
	}

	public String getAuid() {
		return auid;
	}

	public String getDefaultNamespaceName() {
		return defaultNamespaceName;
	}

	public String getDefaultNamespaceValue() {
		return defaultNamespaceValue;
	}

	public String getSchemaLocation() {
		return schemaLocation;
	}

	public String getMimeType() {
		return mimeType;
	}
	
	/**
	 * Method: ensureUniqueness Method to ensure uniqueness as per the
	 * application usage
	 * 
	 * @throws XCAPException
	 */
	public void ensureUniqueness() throws XCAPException {
		// If not unique then 409 with uniqueness-failure
	}; // ensureUniqueness

	/**
	 * Method: constraintFailure Method to check for application usage defined
	 * constraint failure
	 * 
	 * @throws XCAPException
	 */
	public void constraintFailure() throws XCAPException {
		// If some constraint not given by schema are violated then 409 and
		// constraint-failure
	}; // constraintFailure

	
}
