package com.inted.xcap.fe.AppUsages;

/*
 * Class for PresenceRules Application
 */
public class PresenceRulesApp extends AppUsage {

	public PresenceRulesApp() {

		super("presence-rules", "default_namespace", "urn:ietf:params:xml:ns:pres-rules", "presencerules.xsd",
				"pres-rules", "\"" + "application/auth-policy+xml" + "\"");
	};
}
