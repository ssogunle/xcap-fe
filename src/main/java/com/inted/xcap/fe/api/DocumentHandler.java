package com.inted.xcap.fe.api;

import java.util.Iterator;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.namespace.NamespaceContext;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Node;

import com.inted.xcap.fe.AppUsages.AppUsage;
import com.inted.xcap.fe.AppUsages.AppUsageFactory;
import com.inted.xcap.fe.util.SchemaConstants;

public abstract class DocumentHandler implements IDocHandler {

	Logger LOG = LoggerFactory.getLogger(DocumentHandler.class);

	XPath xpath;
	/* Application Usage */
	protected AppUsage appUsage;

	/* auid */
	protected String auid;

	/* It is either users or global */
	protected String documentTree;

	/* XCAP User Identifier :SIP_URL */
	protected String xui;

	/* Document Selector */
	protected String documentSelector;

	/* Actual Document Name */
	protected String documentName;

	/* Node Selector */
	protected String nodeSelector;

	/* XPATH query */
	protected String xpathQuery;

	/* Complete XPATH expression */
	protected String xpathExpression;

	/* Last Path Segment */
	protected String lastPathSegment;

	public DocumentHandler(String xpathExpression, String xpathQuery) {

		this.xpathExpression = xpathExpression;
		this.xpathQuery = xpathQuery;

		XPathFactory xPathfactory = XPathFactory.newInstance();
		xpath = xPathfactory.newXPath();

		lastPathSegment = new String("");

		try {
			parseRequest();
		} catch (Exception ex) {

		}
	}

	protected void parseRequest() throws Exception {

		if (xpathExpression.indexOf("~~") == -1) {
			LOG.error("No node selector");

			documentSelector = xpathExpression;

			if (documentSelector.endsWith("/") == true)
				throw new Exception(HttpServletResponse.SC_BAD_REQUEST + ": Document selector cannot end with /");

		} else {
			String selectors[] = xpathExpression.split("/~~/");

			LOG.info("Length of splitted selector: " + selectors.length);

			if ((selectors.length == 1) || (selectors.length > 2))
				throw new Exception(HttpServletResponse.SC_BAD_REQUEST + ": Check node selector syntax");

			else if (selectors.length == 2) {
				documentSelector = selectors[0];

				LOG.info("Inside: " + documentSelector);

				int lastSlashIndex = selectors[1].lastIndexOf("/");
				if ((lastSlashIndex == -1) && (selectors[1].length() == 0)) {
					throw new Exception(HttpServletResponse.SC_BAD_REQUEST + ": Check node selector syntax");
				} else {
					LOG.info("Inside firstpart: " + selectors[1]);
					LOG.info("Last slash index: " + lastSlashIndex);

					if (lastSlashIndex != -1) {
						lastPathSegment = selectors[1].substring(lastSlashIndex + 1);
						nodeSelector = selectors[1].substring(0, lastSlashIndex);
					} else {
						nodeSelector = selectors[1];
					}

					LOG.info("Inside lastPath: " + lastPathSegment);
					LOG.info("Inside nodePath: " + nodeSelector);

					if ((nodeSelector.matches(".*@xmlns.*") == true)
							|| (lastPathSegment.matches(".*@xmlns.*") == true)) {
						throw new Exception(HttpServletResponse.SC_BAD_REQUEST + ": xmlns not allowed as attribute");
					} else if (lastPathSegment.endsWith("/")) {
						throw new Exception(HttpServletResponse.SC_BAD_REQUEST + ": node attribute cannot end with /");
					}
				}
			}
		}

		LOG.info("docSelector  : " + documentSelector);
		LOG.info("nodeSelector : " + nodeSelector);
		LOG.info("lastSelector : " + lastPathSegment);

		// /service/resource-lists/users/bill/fr.xml/
		String docParts[] = documentSelector.split("/");

		LOG.info("docSelector length: " + docParts.length);
		LOG.info("docSelector: " + docParts[0]);
		LOG.info("docSelector: " + docParts[1]);
		LOG.info("docSelector: " + docParts[2]);

		if (docParts.length >= 5) {
			if (docParts[1].equals((String) "services") == false) {
				throw new Exception(HttpServletResponse.SC_NOT_FOUND + ": XCAP root domain not found");
			} else if ((docParts[3].equals((String) "users") == true)
					|| (docParts[3].equals((String) "global") == true)) {

				documentTree = docParts[3].equals((String) "users") ? "users" : "global";

				auid = docParts[2];
				appUsage = AppUsageFactory.INSTANCE.getApplicationUsage(auid);
			} else
				throw new Exception(HttpServletResponse.SC_BAD_REQUEST + ": DocumentPath users syntax error");

			xui = docParts[4];

			if (docParts.length == 6)
				documentName = docParts[5];

		} else {
			throw new Exception(HttpServletResponse.SC_BAD_REQUEST + ": DocumentPath syntax error");
		}

		LOG.info("+++++++++++++++++++++++++++++++++++++++");
		LOG.info("documentSelector    : " + documentSelector);
		LOG.info("nodeSelector   : " + nodeSelector);
		LOG.info("lastPathSegment: " + lastPathSegment);
		LOG.info("auid           : " + auid);
		LOG.info("documentTree    : " + documentTree);
		LOG.info("xui       : " + xui);
		LOG.info("documentName       : " + documentName);
		LOG.info("xpathExpression           : " + xpathExpression);
		LOG.info("++++++++++++++++++++++++++++++++++++++++\n");

		if (nodeSelector != null) {
			// nodeSelector =
			// bindDefaultNamespaceToQuery(appUsage.getDefaultNamespaceValue(),
			// nodeSelector);
		}
		LOG.info("***************" + lastPathSegment);
		if ((lastPathSegment != null) && (lastPathSegment.equals("") == false)) {
			if ((lastPathSegment.equals(SchemaConstants.NAMESPACE_NS) == false)
					&& (lastPathSegment.startsWith("@") == false)) {
				LOG.info("************Going for Last segment");
				// lastPathSegment =
				// bindDefaultNamespaceToQuery(appUsage.getDefaultNamespaceValue(),
				// lastPathSegment);
			}
		}

		LOG.info("+++++++++++++++++++++++++++++++++++++++");
		LOG.info("documentSelector    : " + documentSelector);
		LOG.info("nodeSelector   : " + nodeSelector);
		LOG.info("lastPathSegment: " + lastPathSegment);
		LOG.info("auid           : " + auid);
		LOG.info("documentTree    : " + documentTree);
		LOG.info("xui       : " + xui);
		LOG.info("documentName       : " + documentName);
		LOG.info("xpathExpression           : " + xpathExpression);
		LOG.info("++++++++++++++++++++++++++++++++++++++++\n");

	}

	protected String bindDefaultNamespaceToQuery(String defaultNamespace, String selector) throws Exception {

		String namespaces[] = new String[SchemaConstants.NAMESPACES_LEN];

		if (xpathQuery.equals("") == false) {
			LOG.info("Found query component");
			String namespace[] = xpathQuery.split("xmlns([a-zA-Z0-9]+=\"[:\\-a-zA-Z0-9:]+\")");
			if (namespace.length > SchemaConstants.NAMESPACES_LEN) {
				throw new Exception(HttpServletResponse.SC_BAD_REQUEST + ": Namespace length limit is 100 in query");
			}
			for (int index = 0; index < namespace.length; index++) {
				System.out.println(namespace[index]);
			}

			if (namespace.length == 0) {
				throw new Exception(HttpServletResponse.SC_BAD_REQUEST + ": Query given wrong");
			} else {
				for (int index = 0; index < namespace.length; index++) {
					System.out.println("Matching query expression: " + namespace[index] + " with: "
							+ "(xmlns\\([a-zA-Z0-9]+=\"[:\\-a-zA-Z0-9:]+\"\\))+");
					if (namespace[index].matches("(xmlns\\([a-zA-Z0-9]+=\"[:\\-a-zA-Z0-9:]+\"\\))+")) {
						String temp[] = namespace[index].split("=");
						int indexOfOpenBraces = temp[0].indexOf("(");
						int indexOfOpenQuotes = temp[1].indexOf("\"");
						int indexOfCloseQoutes = temp[1].indexOf("\"", indexOfOpenQuotes + 1);

						namespaces[index] = temp[0].substring(indexOfOpenBraces + 1);
						System.out.println("Found namespace tag: " + namespaces[index]);
					} else {
						throw new Exception(HttpServletResponse.SC_BAD_REQUEST + ": Query given wrong");
					}
				}
				; // for
			}
			; // length == 0
		}

		String exprParts[] = selector.split("/");

		boolean flag = false;
		String temp = new String();
		for (int index = 0; index < exprParts.length; index++) {
			LOG.info("exprParts[index]" + exprParts[index]);
			if ((exprParts[index].matches("\\*\\[.*\\]\\[.*\\]") == true)
					|| (exprParts[index].matches("\\*\\[.*\\]") == true)
					|| (exprParts[index].matches("\\[.*\\]") == true) || (exprParts[index].matches("~~") == true)
					|| (exprParts[index].equals(SchemaConstants.NAMESPACE_SEL) == true)) {
				temp = temp + exprParts[index];
				if (index != exprParts.length - 1) {
					temp = temp + "/";
				}
				continue;
			} else if (exprParts[index].matches(".*:.*")) {
				System.out.println("Matches .*:.*");
				flag = false;
				String split[] = exprParts[index].split(":");
				if (split[0].matches(".*\\[.*") == true) {
					temp = temp + "def" + ":" + exprParts[index];
				} else {
					LOG.info("Going for given prefix");
					LOG.info(split[0]);
					LOG.info(split[1]);
					for (int x = 0; x < namespaces.length; x++) {
						LOG.info(namespaces[x]);
						if (split[0].equals(namespaces[x]) == true) {
							flag = true;
							break;
						}
						if (namespaces[x] == null) {
							break;
						}
					}
					if (flag == false) {
						throw new Exception(HttpServletResponse.SC_BAD_REQUEST
								+ ": Namspace used in nodeSelector not specified in query"
								+ " OR check attribute syntax");
					} else {
						temp = temp + exprParts[index];
					}
				}
			} else {
				// temp = temp + defaultNamespace + exprParts[index];
				// temp = temp + defaultNamespace + ":" + exprParts[index];
				temp = temp + "def" + ":" + exprParts[index];
			}
			if (index != exprParts.length - 1) {
				temp = temp + "/";
			}
		}
		; // for
		return temp;
		/*
		 * xpath.setNamespaceContext( new NamespaceContext() { public String
		 * getNamespaceURI(String prefix) { switch (prefix) { case "df": return
		 * "http://xml.sap.com/2002/10/metamodel/webdynpro"; } return null; }
		 * 
		 * @Override public String getPrefix(String namespaceURI) { // TODO
		 * Auto-generated method stub return null; }
		 * 
		 * @Override public Iterator getPrefixes(String namespaceURI) { // TODO
		 * Auto-generated method stub return null; } });
		 * 
		 * return null;
		 */
	}

	/**
	 * Method: processGetRequest Method to process implementation specific get
	 * request
	 * 
	 * @param mimetype
	 *            specifies the mimetype
	 * @return Vector specifies a vector or result and mime-type
	 * @throws XCAPException
	 */
	abstract public Vector processGetRequest(String mimetype) throws Exception;

	/**
	 * Method: getDocument method to get the result for the given XCAP
	 * 
	 * @param req
	 *            HttpServletRequest
	 * @param resp
	 *            HttpServletResponse
	 * @throws XCAPException
	 */
	public void getDocument(HttpServletRequest req, HttpServletResponse resp) throws Exception {
		throw new Exception(HttpServletResponse.SC_INTERNAL_SERVER_ERROR + ": Fatal");
	}; // getDocument

	/**
	 * Method: putDocument Abstract method to put the given document position
	 * pointed by the XCAP expression
	 * 
	 * @param doc
	 *            specifies the document to be inserted.
	 * @param expr
	 *            specifies the XCAP expression where the document has to be
	 *            inserted.
	 * @throws XCAPException
	 */
	public boolean putDocument(HttpServletRequest req, HttpServletResponse resp) throws Exception {
		throw new Exception(HttpServletResponse.SC_INTERNAL_SERVER_ERROR + ": Fatal");
	}; // putDocument

	/**
	 * Method: deleteDocument Abstract method to delete the entry pointed by the
	 * given XCAP
	 * 
	 * @throws XCAPException
	 */
	public void deleteDocument() throws Exception {
		throw new Exception(HttpServletResponse.SC_INTERNAL_SERVER_ERROR + ": Fatal");
	}; // deleteDocument

	/**
	 * Method: getAppUsage Method to get the application usage for this document
	 * 
	 * @return AppUsage specifies the application usage for this document
	 * @throws XCAPException
	 */
	public AppUsage getAppUsage() {
		return appUsage;
	}; // getAppUsage

	/**
	 * Method: calcETag Method to calculate Etag Returns a random etag everytime
	 * this is invoked.
	 * 
	 * @param object
	 *            specifies the object to be used based on which etag is
	 *            calculated
	 * @return String specifying the ETag calculated. This is left for the
	 *         derived classes to implement.
	 * @throws XCAPException
	 */
	public String calcETag(Object object) throws Exception {
		throw new Exception(HttpServletResponse.SC_INTERNAL_SERVER_ERROR + ": Fatal");
	}; // calcETag

	/**
	 * Method: getDocumentSelector Mwthod to get the document selector
	 * 
	 * @return String specifying the document selector
	 */
	public String getDocumentSelector() {
		return documentSelector;
	}; // getDocumentSelector

	/**
	 * Method: getModifyTime Abstract function for the store implementors to
	 * implement. Should return the last modified time of the document being
	 * processed.
	 * 
	 * @param String
	 *            specifying the last modified time of the document being
	 *            processed.
	 */
	public abstract String getModifyTime();

	/**
	 * Method: getNewNode Test method, not to be used or implemented
	 */
	public abstract Node getNewNode(String lastModTime);

}
