package com.inted.xcap.fe.api;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.StringReader;
import java.security.MessageDigest;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.SimpleTimeZone;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.inted.xcap.fe.Put;
import com.inted.xcap.fe.AppUsages.AppUsage;
import com.inted.xcap.fe.error.CannotDelete;
import com.inted.xcap.fe.error.NoParent;
import com.inted.xcap.fe.error.NullError;
import com.inted.xcap.fe.parsers.DOMXMLParser;
import com.inted.xcap.fe.parsers.SaxonParser;
import com.inted.xcap.fe.parsers.SchemaValidator;
import com.inted.xcap.fe.util.Base64Coder;
import com.inted.xcap.fe.util.NotifyChange;
import com.inted.xcap.fe.util.NotifyChangeMgr;
import com.inted.xcap.fe.util.ResultObject;
import com.inted.xcap.fe.util.SchemaConstants;
import com.inted.xcap.fe.util.XCAPException;

public class FileDocumentHandler  extends DocumentHandler {

	Logger LOG = LoggerFactory.getLogger(FileDocumentHandler.class);
	/** 
	   * Method: FileDocumentHandler
	   *         Construct and check the documentHandler semantics and syntax
	   * @param  expr specifies the XCAP expression
	   * @param  query specifies the Query (namespace binding) in the XPATH
	   *         URI
	   * @throws XCAPException        
	   */
	
	private String xml;
	private  NotifyChange m_notifyChange;
	
	public FileDocumentHandler(String xPathExpression, String xPathQuery)
	{
		    super(xPathExpression, xPathQuery);
		 //   m_notifyChange = NotifyChangeMgr.getInstance().getNotifyChange();

	}

	  /**
	   * Method: getNewNode
	   *         Not meant to be used, just a test method
	   * @return Node always NULL        
	   */
	  public Node getNewNode(String lastModTime)
	  {
	    Node node = null;
	    return node;
	  }; // getNewNode


	  /**
	   * Method: getEntry
	   *         Private method to get the specific DOM Node
	   * @return String specifying the result of the get operaiton
	   * @throws XCAPException
	   */
	  //--------------------------------------------------------------------------
	  private String getEntry()
	    throws XCAPException
	  {
	    String result = null;
	    FileInputStream fileIpStr = null;
	    try
	    {
	    	/*
	    	 * Fetch file from Database!!!!!!!!
	    	 */
	      fileIpStr = new FileInputStream(documentSelector);
	    }
	    catch(java.io.FileNotFoundException exception)
	    {
	       throw new XCAPException(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, 
	          new NullError(SchemaConstants.NULLSCHEMA, "Document not found"));
	    }

	    Document doc = null;
	    NodeList nodeList = null;

	    DOMXMLParser domXMLParser = new DOMXMLParser(true);
	      doc = (Document)domXMLParser.parse(fileIpStr);

	      SaxonParser parser = new SaxonParser();
	      if(lastPathSegment.equals(SchemaConstants.NAMESPACE_SEL))
	      {
	        result = parser.evaluateNamespaces(nodeSelector, xpathQuery, documentSelector, 
	            appUsage);
	        System.out.println("GOT NAMESPACES: " + result);
	      }
	      else
	      {
	        if((lastPathSegment.equals("") == false) && (lastPathSegment != null))
	        {
	          nodeList = (NodeList)parser.evaluateXPATH(nodeSelector + "/" +
	              lastPathSegment, xpathQuery, doc, appUsage);
	        }
	        else
	        {
	          nodeList = (NodeList)parser.evaluateXPATH(nodeSelector, xpathQuery, 
	              doc, appUsage);
	        }
	      }

	    if(lastPathSegment.equals(SchemaConstants.NAMESPACE_SEL) == false)
	    {
	      if(nodeList.getLength() != 1)
	      {
	        throw new XCAPException(HttpServletResponse.SC_NOT_FOUND, 
	            new NullError(SchemaConstants.NULLSCHEMA, 
	              "No or more than one elements found"));
	      }
	      try
	      {
	        if(nodeList.item(0).getNodeType() == Node.ATTRIBUTE_NODE)
	        {
	          LOG.info("******************");
	          LOG.info("******************");
	          LOG.info("******************");
	          LOG.info("******************");
	          LOG.info("******************");
	          
	          Attr attr = (Attr)nodeList.item(0);
	          
	          String attrName  = attr.getName();
	          String attrValue = attr.getValue();
	          result = attrName + ":" + attrValue;
	        }
	        else
	        {
	          result = SaxonParser.print(nodeList.item(0), "Get returned");
	        }
	      }
	      catch(Exception exp)
	      {
	        throw new XCAPException(HttpServletResponse.SC_CONFLICT , 
	            new NullError(SchemaConstants.NULLSCHEMA, "some error"));
	      }
	    }
	  
	    return result;

	  }; // getEntry
	
	  

	  /**
	   * Method: processGetRequest
	   *         Method to process file specific get request
	   * @param  String specifying the mimetype        
	   * @return Vector specifies the result of the get operation
	   * @throws XCAPException
	   */
	  public Vector processGetRequest(String mimeType)
	    throws XCAPException
	  {
	    String result = new String("");
	    if(nodeSelector != null)
	    {
	      result = getEntry();
	      if(result == null)
	      {
	        throw new XCAPException(HttpServletResponse.SC_BAD_REQUEST, 
	            new NullError(SchemaConstants.NULLSCHEMA, "Nothing found"));
	      }
	      else
	      {
	        if(lastPathSegment.startsWith("@") == true)
	        {
	          mimeType = SchemaConstants.ATT_NS;
	        }
	        else if(lastPathSegment == SchemaConstants.NAMESPACE_SEL)
	        {
	          mimeType = SchemaConstants.NAMESPACE_NS;
	        } 
	        else
	        {
	          mimeType = SchemaConstants.ELEMENT_NS;
	        }
	      }
	    }
	    else
	    {
	      LOG.info("Got NULL NODESELECTOR");
	      try
	      {
	        // read from file and return result
	        BufferedReader reader = new BufferedReader(new StringReader(xml));
	        String temp;
	        do
	        {
	         temp = reader.readLine();
	         if(temp != null)
	         {
	           result = result + temp + "\n"; 
	         }
	        }while(temp != null);
	        mimeType = appUsage.getMimeType();
	        LOG.info("mimeType: " + mimeType);
	      }
	      catch(java.io.FileNotFoundException exception)
	      {
	        throw new XCAPException(HttpServletResponse.SC_BAD_REQUEST, 
	            new NullError(SchemaConstants.NULLSCHEMA, "Document not found"));
	      }//---------------------------------------------------------------------
	      catch(java.io.IOException exception)
	      {
	        throw new XCAPException(HttpServletResponse.SC_BAD_REQUEST, 
	            new NullError(SchemaConstants.NULLSCHEMA, "Document not readable"));
	      }
	    }
	    Vector ret = new Vector();
	    ret.add(result);
	    ret.add(mimeType);
	    return ret;
	  }
	
	  /**
	   * Method: getDocument 
	   *         public method to get the result for the given XCAP
	   * @return String specifying the String representation of the returned
	   *         document
	   * @throws XCAPException
	   */
	  public String getDocument()
	    throws XCAPException
	  {
	    String mimeType = getAppUsage().getMimeType();  
	    xml = null;
	    String result = new String();
	    if(documentTree.equals("users") )
	    {
	      if(documentName != null)
	      {
	        LOG.info("FILE PATH: " + documentSelector);
	        
	        
	        xml = ""; ////This is where the XML Document is passed!!!!!
	        
	        if(xml == null || xml.isEmpty())
	        {
	          throw new XCAPException(HttpServletResponse.SC_NOT_FOUND,
	              new NullError(SchemaConstants.NULLSCHEMA, "Document not found"));
	        }
	      }
	      else
	      {
	        throw new XCAPException(HttpServletResponse.SC_NOT_FOUND, 
	            new NullError(SchemaConstants.NULLSCHEMA, "Document not found"));
	      }
	    }
	    else
	    {
	      // do something with global
	    }

	    String mimetype = new String("");
	    Vector retType = processGetRequest(mimetype);
	    result = (String)retType.elementAt(0);
	   LOG.info("mimeType: " + (String)retType.elementAt(1) + "\n");
	    return result;
	    //--------------------------------------------------------------------------
	  };
	  
	  
	  /**
	   * Method: getDocument 
	   *         public method to get the result for the given XCAP
	   * @param  req HttpServletRequest
	   * @param  resp HttpServletResponse
	   * @throws XCAPException
	 * @throws IOException 
	   */
	  public void getDocument(HttpServletRequest req, HttpServletResponse resp) 
	    throws XCAPException, IOException
	  {
	    String mimeType = getAppUsage().getMimeType();  
	    xml = null;
	    String result = null;
	    if(documentTree.equals("users"))
	    {
	      if(documentName != null)
	      {
	       //_file = new File(AppUsage.BASE+_docSelector);
	       // if(_file.exists() == false)
	    	     if(xml == null || xml.isEmpty())  
	        {
	          throw new XCAPException(HttpServletResponse.SC_NOT_FOUND,
	              new NullError(SchemaConstants.NULLSCHEMA, "Document not found"));
	        }
	      }
	      else
	      {
	        throw new XCAPException(HttpServletResponse.SC_NOT_FOUND, 
	            new NullError(SchemaConstants.NULLSCHEMA, "Document not found"));
	      }
	    }
	    else
	    {
	      // do something with global
	    }

	    resp.setHeader("Last-Modified", getModifyTime());
//	 /   String mimeType = new String("");
	    Vector retType = processGetRequest(mimeType);

	    resp.setContentType((String)retType.elementAt(1));
	    resp.addHeader("Server", "Inted XCAPServer");
	    
	    LOG.info((String)retType.elementAt(0));
	    
	    resp.setContentLength(((String)retType.elementAt(0)).length());
	    java.io.PrintWriter out = resp.getWriter();
	    out.write((String)retType.elementAt(0));
	    out.close();
	    //--------------------------------------------------------------------------
	  }
	  
	  /**
	   * Method: getModifyTime
	   *         Specifies the time when the File was last modified
	   * @param  file specifies the File whose modification date has to be
	   *         retrieved
	   * @return String specifies the time in 
	   *           "Tue, 15 Nov 1994 12:45:26 GMT" 
	   *           "EEE, d MMM yyyy HH:mm:ss 'GMT'"
	   *         format.
	   */
	  public String getModifyTime()
	  {
	    File file = new File(documentSelector);
	    SimpleDateFormat dateFormat = new SimpleDateFormat("EEE, d MMM yyyy HH:mm:ss 'GMT'");
	    dateFormat.setTimeZone(new SimpleTimeZone(0, "GMT"));
	    Date date = new Date(file.lastModified());
	    return (String)dateFormat.format(date);
	  }
	  
	  
	  /**
	   * Method: putDocument
	   *         Method to put the given document position pointed by the 
	   *         XCAP expression
	   * @param  HttpServletRequest specifies the XCAP PUT request
	   * @param  HttpServletResponse specifies the XCAP PUT response
	   * @throws XCAPException
	   */
	  //----------------------------------------------------------------------------
	  public boolean putDocument(HttpServletRequest req, HttpServletResponse resp)
	    throws XCAPException
	  {
	    System.out.println("-->putDocument");

	    String temp = new String("");
	    String body = new String("");
	    try
	    {
	      BufferedReader in = req.getReader();
	      temp = in.readLine();
	      if(temp != null)
	      {
	        body += temp;
	        while((temp = in.readLine()) != null) 
	        {
	          body += temp;
	        }
	        in.close();
	      }
	      else
	      {
	        throw new XCAPException(HttpServletResponse.SC_BAD_REQUEST,
	            new NullError(SchemaConstants.NULLSCHEMA, "Put without body"));
	      }
	    }
	    catch(java.io.IOException exception)
	    {
	      throw new XCAPException(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, 
	          new NullError(SchemaConstants.NULLSCHEMA, "Error reading body"));
	    }
	    System.out.println("++Body: " + body);
	 
	    System.out.println("************Body" + body);
	    System.out.println("Read file");
	    String mimeType = req.getContentType();
	    System.out.println("Mimetype: " + mimeType);
	    if(mimeType == null)
	    {
	      throw new XCAPException(HttpServletResponse.SC_BAD_REQUEST,
	          new NullError(SchemaConstants.NULLSCHEMA, "MimeType Null"));
	    }

	    boolean newCreation = putDocument(body, mimeType);
	    if(newCreation == true)
	    {
	 //     String etag = calcETag(AppUsage.BASE + getDocumentSelector());
	    	String etag = calcETag(getDocumentSelector());
	      resp.setHeader("ETag", "\"" + etag + "\"");
	    }
	    System.out.println("putDocument-->");
	    return newCreation;
	  }; // putDocument



	  /**
	   * Method: calcETag
	   *         Method to calculate Etag
	   * @return Object Object whose ETag has to be calculated from a MD5 hash
	   * @throws XCAPException
	   */
	  public String calcETag(Object object)
	    throws XCAPException
	  {
	    String encodedDigest = null;
	    try
	    {
	      // Create a Message Digest from a Factory method
	      MessageDigest md = MessageDigest.getInstance("MD5");

	      // Create the message, convert the file contents to an array of bytes[]
	      File file = new File((String)object);
	      byte[] msg = getBytesFromFile(file);

	      // Update the message digest with some more bytes
	      // This can be performed multiple times before creating the hash
	      md.update(msg); 

	      // Create the digest from the message
	      byte[] MessageDigest = md.digest();

	      String ans = new String(MessageDigest); 
	      // Human readable form
	      encodedDigest = new String(Base64Coder.encode(MessageDigest));

	      //encodedDigest = new String(MessageDigest);
	    }
	    catch(java.security.NoSuchAlgorithmException exception)
	    {
	      throw new XCAPException(HttpServletResponse.SC_INTERNAL_SERVER_ERROR,
	          new NullError(SchemaConstants.NULLSCHEMA, "Could not use MD5"));
	    }

	    return (encodedDigest);
	  }; // calcETag

	 
	  /** 
	   * Method: getBytesFromFile
	   *         Method to read bytes from a file
	   * @param  File specifying the File to be read 
	   * @return byte[] specifies the bytes read from teh file
	   * @throws XCAPException
	   */         
	  byte[] getBytesFromFile(File file)
	    throws XCAPException
	  {
	    InputStream is = null;
	    try
	    {
	      is = new FileInputStream(file);
	    }
	    catch(java.io.FileNotFoundException exception)
	    {
	      throw new XCAPException(HttpServletResponse.SC_NOT_FOUND,
	          new NullError(SchemaConstants.NULLSCHEMA, "Could not read file"));
	    }

	    long length = file.length();

	    if(length > Integer.MAX_VALUE) 
	    {
	      throw new XCAPException(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, 
	          new NullError(SchemaConstants.NULLSCHEMA, "File to long to read"));
	    }

	    byte[] bytes = new byte[(int)length];

	    try
	    {
	      int offset = 0;
	      int numRead = 0;
	      while((offset < bytes.length) && 
	          (numRead=is.read(bytes, offset, bytes.length-offset)) >= 0) 
	      {
	        offset += numRead;
	      }

	      if(offset < bytes.length) 
	      {
	        throw new XCAPException(HttpServletResponse.SC_INTERNAL_SERVER_ERROR,
	            new NullError(SchemaConstants.NULLSCHEMA, 
	              "Could not completely read file"));
	      }
	      is.close();
	    }
	    catch(java.io.IOException exception)
	    {
	      throw new XCAPException(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, 
	          new NullError(SchemaConstants.NULLSCHEMA, 
	            "Could not completely read file"));
	    }

	    return bytes;
	  }; // getBytesFromFile 

	  
	  /** 
	   * Method: createFile
	   *         Method to create a file given the file path and the file body
	   * @param  path String specifying the path of the output File
	   * @param  body String specifying the body for the file
	   * @throws XCAPException
	   */ 
	  // --------------------------------------------------------------------------
	  void createFile(String path, String body)
	    throws XCAPException
	  {
	    try
	    {
	      File file = new File(path);
	      System.out.println("path: " + path);
	      if(file.createNewFile() == true)
	      {
	        System.out.println("New file created");
	      } 
	      else 
	      {
	        System.out.println("File exists, will overwrite");
	      }
	      FileOutputStream fOS = new FileOutputStream(file);
	      PrintWriter writer = new PrintWriter(fOS);
	      writer.print(body);
	      writer.close();
	    }
	    catch(Exception exception)
	    {
	      throw new XCAPException(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, 
	          new NullError(SchemaConstants.NULLSCHEMA, "Could not write file"));
	    }

	      NotifyChange notifyChange = 
	        NotifyChangeMgr.getInstance().getNotifyChange();
	    if(notifyChange != null)
	    {
	      if(NotifyChangeMgr.getInstance().getNotifyChange().getError() == false)
	      {
	        try
	        {
	          m_notifyChange.getSocket().write(documentSelector);
	        }
	        catch(XCAPException exception)
	        {
	          System.out.println("NO PA CONNECTED");
	        }
	      }
	      else
	      {
	        System.out.println("COULD NOT PROPOGATE CHANGE TO PA");
	      }
	    }
	    else
	    {
	      System.out.println("NOT CONFIGURED TO PROPOGATE CHANGE TO PA");
	    }

	  } // createFile
	  
	  
	  /**
	   * Method: putDocument
	   *         Method to put the given document 
	   * @param  String specifies the document to be inserted.
	   * @param  String specifies the mimetype of the body to be inserted
	   * @return boolean specifying whether the insertion in a new one or an
	   *         update. True means new insertion
	   * @throws XCAPException        
	   */
	  public boolean putDocument(String reqBody, String mimeType)
	    throws XCAPException
	  {
	    System.out.println("-->Another put");
	    boolean newCreation = false;
	  //  File file = new File(AppUsage.BASE+_docSelector);
	    File file = new File(documentSelector);
	    File dir = file.getParentFile();

	    System.out.println("File name: " + file.getAbsolutePath());
	    System.out.println("Dir name : " + dir.getAbsolutePath());

	    /**
	     * 1. parent directory NE, file E,  node selector E  --> error 409
	     * 2. parent directory NE, file E,  node selector NE --> error 409
	     * 3. parent directory NE, file NE, node selector E  --> error 409
	     * 4. parent directory NE, file NE, node selector NE --> error 409
	     */
	    if((dir == null) || (dir.exists() == false))
	    {
	      throw new XCAPException(HttpServletResponse.SC_CONFLICT, 
	          new NoParent(SchemaConstants.NULLSCHEMA, "parent not found"));
	    }
	    /**
	     * 5. parent directory E,  file NE, node selector E  --> error 409
	     */
	    else if((nodeSelector != null) && (file.exists() == false))
	    {
	      throw new XCAPException(HttpServletResponse.SC_CONFLICT, 
	          new NoParent(SchemaConstants.NULLSCHEMA, "file not found"));
	    }
	    else 
	    {
	      String body = new String(reqBody);
	      /** 
	       * Only XCAP root. Avert DOS attacks as per application usage inherent
	       * weaknesses.
	       */
	      if((nodeSelector == null) && (file.exists() == false) && 
	          (dir.getName() == appUsage.getAppName()))
	      {
	        throw new XCAPException(HttpServletResponse.SC_CONTINUE,
	          new NullError(SchemaConstants.NULLSCHEMA, "file not found"));
	      }
	      else
	      {
	        if(nodeSelector == null)
	        {
	          // check for mime-types
	        	LOG.info("mimeType: " + mimeType);
	        	LOG.info("_appUsage.getMimeType: " + 
	              appUsage.getMimeType());
	          if(mimeType.equals(appUsage.getMimeType()) == false)
	          {
	            throw new XCAPException(
	                HttpServletResponse.SC_UNSUPPORTED_MEDIA_TYPE, 
	                new NullError(SchemaConstants.NULLSCHEMA, "Wrong media type"));
	          }

	          // content of the request body must be well-formed
	          SchemaValidator schemaValidator = 
	            new SchemaValidator(SchemaConstants.DOCUMENT);
	          schemaValidator.validateSchema(appUsage.getSchemaLocation(), body);

	          // utf-8 ???

	          /**
	           * 6. parent directory E,  file NE, node selector NE 
	           *    --> create file
	           * 7. parent directory E,  file E,  node selector NE 
	           *    --> replace file
	           */
	          LOG.info("Creating a file");
	        ///  createFile(AppUsage.BASE+_docSelector, body);
	          newCreation = true;
	        }
	        else
	        {
	        	LOG.info("MIMETYPE: " + mimeType);
	          if((mimeType.equals(SchemaConstants.ELEMENT_NS) == false) &&
	              (mimeType.equals(SchemaConstants.ATT_NS) == false))
	          {
	            throw new XCAPException(
	                HttpServletResponse.SC_UNSUPPORTED_MEDIA_TYPE,
	                new NullError(SchemaConstants.NULLSCHEMA, "Wrong media type"));
	          }
	          else
	          {
	            /**
	             * @todo [Anurag]
	             * 1. utf-8
	             */

	            /**
	             * 8. parent directory E,  file E,  node selector E 
	             *    --> selector no-match || selector invalid
	             *      --> create depends on context node
	             *    --> selector match
	             *      --> replace depends on context node
	             */
	            try
	            {
	            	LOG.info("parent directory E,  file E,  node selector E");
	              Put putEngine = new Put();
	              FileInputStream fileIpStr = 
	              //  new FileInputStream(AppUsage.BASE + documentSelector);
	            		  new FileInputStream(documentSelector);
	              ResultObject resultObject = putEngine.putEntry(nodeSelector, 
	                  lastPathSegment, xpathQuery, fileIpStr, body, appUsage, 
	                  mimeType);

	          //    createFile(AppUsage.BASE+documentSelector, resultObject.getResult());
	              createFile(documentSelector, resultObject.getResult());
	              newCreation = resultObject.getNewCreation();
	            }
	            catch(java.io.FileNotFoundException Exception)
	            {
	              throw new XCAPException(HttpServletResponse.SC_INTERNAL_SERVER_ERROR,
	                  new NullError(SchemaConstants.NULLSCHEMA, "Cannot find the item pointed by Document Selector"));
	            }
	          }
	        }
	      }; // end of one big put
	      
	    }; // put finished phew..
	    LOG.info("FileDocumentHandler::newCreation " + newCreation);
	    
	    
	    return newCreation;
	  }; // putDocument

	
	  /**
	   * Method: deleteDocument
	   *         Method to delete a document for the local store implementation
	   * @throws XCAPException        
	   */
	  public void deleteDocument()
	    throws XCAPException
	  {
	    if(lastPathSegment != null)
	    {
	      System.out.println("++++++++++++_lastPathSegment: " + lastPathSegment);
	      System.out.println("++++++++++++_lastPathSegment: " + SchemaConstants.NAMESPACE_SEL);
	      if(lastPathSegment.equals(SchemaConstants.NAMESPACE_SEL) == true)
	      {
	        throw new XCAPException(HttpServletResponse.SC_METHOD_NOT_ALLOWED, 
	            new NullError(SchemaConstants.NULLSCHEMA, 
	              "Namespace selector not allowed in delete method"));
	      } // Namspace selector
	    }
	   // File file = new File(AppUsage.BASE+_docSelector);
	    File file = new File(documentSelector);
	    if(file.exists())
	    {
	      if(nodeSelector == null)
	      {
	        try
	        {
	          if(file.delete() == false)
	          {
	            throw new XCAPException(HttpServletResponse.SC_NOT_FOUND,
	              new NullError(SchemaConstants.NULLSCHEMA, 
	                "Document to be deleted not found"));
	          }; // delete failed
	        }
	        catch(java.lang.SecurityException securityException)
	        {
	          throw new XCAPException(HttpServletResponse.SC_NOT_FOUND,
	            new NullError(SchemaConstants.NULLSCHEMA, 
	              "Security violated"));
	        }; // catch if delete failed
	      } // only docSelector
	      else
	      {
	        // form dom document
	        String result = null;
	        FileInputStream fileIpStr = null;
	        try
	        {
	         // fileIpStr = new FileInputStream(AppUsage.BASE+_docSelector);
	        	   fileIpStr = new FileInputStream(documentSelector);
	        }
	        catch(java.io.FileNotFoundException exception)
	        {
	           throw new XCAPException(
	               HttpServletResponse.SC_INTERNAL_SERVER_ERROR, 
	               new NullError(SchemaConstants.NULLSCHEMA, 
	                 "Document ont found"));
	        }
	        
	        // Form DOM document of the file read
	        DOMXMLParser xmlParser = new DOMXMLParser(true);
	        Document doc = (Document)xmlParser.parse(fileIpStr);

	        // evaluate XPATH to find document to delete
	        SaxonParser parser = new SaxonParser();
	        NodeList nodeList = null;
	        if((lastPathSegment.equals("") == false) && 
	            (lastPathSegment != null))
	        {
	          nodeList = (NodeList)parser.evaluateXPATH(nodeSelector +  "/" +
	            lastPathSegment, xpathQuery, doc, appUsage);
	        }
	        else
	        {
	          nodeList = (NodeList)parser.evaluateXPATH(nodeSelector,
	              xpathQuery, doc, appUsage);
	        }

	        if(nodeList.getLength() != 1)
	        {
	          throw new XCAPException(HttpServletResponse.SC_NOT_FOUND,
	            new NullError(SchemaConstants.NULLSCHEMA, 
	              "Node to be deleted not found"));
	        }
	        else
	        {
	          try
	          {
	            Node adopted = doc.importNode(nodeList.item(0), true);
	            SaxonParser.print(adopted, "Node to be deleted");
	            // keep the surrounding namespaces ??
	            System.out.println("NodeName to be deleted: " + 
	                adopted.getNodeName());

	            if(adopted.getNodeType() == Node.ATTRIBUTE_NODE)
	            {
	              System.out.println("ATTR DELETION");
	              System.out.println("ATTR DELETION");
	              System.out.println("ATTR DELETION");
	              NodeList deletionList = (NodeList)parser.evaluateXPATH(
	                  nodeSelector, xpathQuery, doc, appUsage);
	              if(deletionList.getLength() != 1)
	              {
	                throw new XCAPException(
	                    HttpServletResponse.SC_CONFLICT,
	                    new CannotDelete(SchemaConstants.NULLSCHEMA, 
	                    "Cannot delete 0 or more than 1 attrbutes of dif nodes"));
	              }
	              else
	              {
	                Attr attr = (Attr)adopted;
	                Element deleteAttElement = (Element)deletionList.item(0);
	                deleteAttElement.removeAttribute(attr.getName());
	              }
	            }
	            else
	            {
	              NodeList matchingList = doc.getElementsByTagName(
	                  adopted.getNodeName());

	             LOG.info(""+matchingList.getLength());

	              for(int index=0; index<matchingList.getLength(); index++)
	              {
	                if(matchingList.item(index).isEqualNode(adopted))
	                {
	                  if(lastPathSegment.startsWith("@") == true)
	                  {
	                  }
	                  else
	                  {
	                	  LOG.info("GOT THE NODE TO BE DELETED");
	                    Element parent = 
	                      (Element)matchingList.item(index).getParentNode();
	                    parent.removeChild(matchingList.item(index));
	                    SaxonParser.print(doc, "DELETE WORKS");
	                    break;
	                  }
	                }
	                LOG.info("OOPS");
	              }
	            }

	            LOG.info("*********************");
	            LOG.info("*********************");

	          }
	          catch(org.w3c.dom.DOMException exception)
	          {
	            throw new XCAPException(
	                HttpServletResponse.SC_INTERNAL_SERVER_ERROR, 
	                new NullError(SchemaConstants.NULLSCHEMA, 
	                "deletion exception " + exception.getMessage()));
	          }
	           
	          // see if the xpath again selects something
	          if((lastPathSegment.equals("") == false) && (lastPathSegment != null))
	          {
	            nodeList = (NodeList)parser.evaluateXPATH(nodeSelector +  "/" +
	              lastPathSegment, xpathQuery, doc, appUsage);
	          }
	          else
	          {
	            nodeList = (NodeList)parser.evaluateXPATH(nodeSelector,
	                xpathQuery, doc, appUsage);
	          }

	          if(nodeList.getLength() != 0)
	          {
	            throw new XCAPException(HttpServletResponse.SC_CONFLICT,
	              new CannotDelete(SchemaConstants.NULLSCHEMA, 
	                "Delete just did not work"));
	          }
	          SchemaValidator schemaValidator = 
	            new SchemaValidator(SchemaConstants.WHOLE);
	          schemaValidator.validateSchema(appUsage.getSchemaLocation(), doc);

	          appUsage.ensureUniqueness();
	          // leaving out contraint failure for now same in PUT
	          result = SaxonParser.print(doc, "Finally");
	          //createFile(AppUsage.BASE+_docSelector, result);
	        }
	      } // nodeslector
	    } // file exists
	    else
	    {
	      throw new XCAPException(HttpServletResponse.SC_NOT_FOUND,
	          new NullError(SchemaConstants.NULLSCHEMA, 
	            "Document to be deleted not found"));
	    }; // file does not exist
	  }; // deleteDocument
}
