package com.inted.xcap.fe.api;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
	 * Class forms an interface class for different store implementations
	 */
	public interface IDocHandler {
		/**
		 * Method: getDocument method to get the result for the given XCAP
		 * 
		 * @param req
		 *            HttpServletRequest
		 * @param resp
		 *            HttpServletResponse
		 * @throws XCAPException
		 */
		public void getDocument(HttpServletRequest req, HttpServletResponse resp) throws Exception;

		/**
		 * Method: putDocument Abstract method to put the given document given
		 * in the XCAP request
		 * 
		 * @param req
		 *            HttpServletRequest
		 * @param resp
		 *            HttpServletResponse
		 * @throws XCAPException
		 */
		public boolean putDocument(HttpServletRequest req, HttpServletResponse resp) throws Exception;

		/**
		 * Method: deleteDocument Abstract method to delete document specified
		 * by the XCAP URI
		 * 
		 * @throws XCAPException
		 */
		public void deleteDocument() throws Exception;


}
