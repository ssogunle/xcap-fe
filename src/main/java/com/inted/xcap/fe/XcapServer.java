package com.inted.xcap.fe;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.inted.xcap.fe.api.DocumentHandler;
import com.inted.xcap.fe.api.FileDocumentHandler;
import com.inted.xcap.fe.error.NullError;
import com.inted.xcap.fe.util.HttpSemantics;
import com.inted.xcap.fe.util.SchemaConstants;
import com.inted.xcap.fe.util.XCAPException;


/**
 * Servlet implementation class XcapServer
 */
public class XcapServer extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	Logger LOG = LoggerFactory.getLogger(XcapServer.class);
	
	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public XcapServer() {
		super();

	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		try{
		/** Get the XCAP URI info */
		String docNode = request.getPathInfo();
		String contextPath = request.getContextPath();
		String pathTranslated = request.getPathTranslated();
		String query = request.getQueryString();

		/** Set query to "" if not specfied in XCAP URI */
		if (query == null) {
			LOG.info("NULL QUERY");
			query = new String("");
		}

		/** document Selector not specified */
		if (docNode == null) {
			 throw new XCAPException(HttpServletResponse.SC_NOT_FOUND, 
			            new NullError(SchemaConstants.NULLSCHEMA, 
			              "XCAP expression not specified"));
		}

		/**
		 * Initialize the Document Handler through FileHandler specialization
		 */
		DocumentHandler docHandler = new FileDocumentHandler(docNode, query);

		/** Check the HttpScemantics of the XCAP Request */
		
		HttpSemantics httpSemantics = new HttpSemantics();
		if (httpSemantics.conditionalSemantics(request, response, docHandler) == true) {
			/** Process get request */
			docHandler.getDocument(request, response);
		} else /** HttpSemantics check failed -- mostly ETag related */
		{
			throw new XCAPException(HttpServletResponse.SC_INTERNAL_SERVER_ERROR,
					new NullError(SchemaConstants.NULLSCHEMA, "Conditional handling flawed"));
		}
		
		}catch(Exception ex){
			
		}
	}
	/**
	   * Method: doPut
	   *         Called by the server (via the service method) to allow a
	   *         servlet to handle a PUT request. 
	   *         http://java.sun.com/j2ee/sdk_1.3/techdocs/api/javax/servlet/http/
	   *             HttpServlet.html#doGet(javax.servlet.http.HttpServletRequest,
	   *             javax.servlet.http.HttpServletResponse)
	   * @param  req the HttpServletRequest
	   * @param  resp the HttpServletResponse
	   * @throws ServletException
	   * @throws IOException
	   */
	  protected void doPut(HttpServletRequest req, HttpServletResponse resp) 
	    throws ServletException, IOException 
	  {
	    LOG.info("-->doPut");
	    disableCache(resp);
	    try
	    {
	      /** Get the XCAP URI info */
	      /**
	       * @NOTE IMPORTANT:
	       * Strings are non-mutable references in java, maybe we could make
	       * this global for this servelt thread to avoid this code repeatation
	       * --> Use call back registered handler to simulate function pointer
	       *  and get the needless repeation out.
	       * --> It is ok for immediate use but not for future use  
	       */ 
	      String docNode = req.getPathInfo();
	      String contextPath = req.getContextPath();
	      String pathTranslated = req.getPathTranslated();
	      String query = req.getQueryString();

	      /** document Selector not specified */
	      if(docNode == null) 
	      {
	        throw new XCAPException(HttpServletResponse.SC_NOT_FOUND, 
	            new NullError(SchemaConstants.NULLSCHEMA, 
	              "XCAP expression not specified"));
	      }

	      /** Set query to "" if not specfied in XCAP URI */
	      if(query == null)
	      {
	    	  LOG.info("NULL QUERY");
	        query = new String("");
	      }

	      /** 
	       * Initialize the Document Handler through FileHandler specialization
	       */
	      DocumentHandler docHandler = new FileDocumentHandler(docNode, query);

	      /** Check the HttpScemantics of the XCAP Request */
	      HttpSemantics httpSemantics = new HttpSemantics();
	      if(httpSemantics.conditionalSemantics(req, resp, docHandler) == true)
	      {
	    	  LOG.info("+++++GOING FOR NEW PUT+++++");
	        /** Process PUT request */
	        boolean newCreation = docHandler.putDocument(req, resp);

	        /** Set response message as per new creation or update */
	        if(newCreation)
	        {
	          resp.setStatus(HttpServletResponse.SC_CREATED);
	        }
	        else
	        {
	           resp.setStatus(HttpServletResponse.SC_OK);
	        }
	      }
	      else
	      {
	        throw new XCAPException(HttpServletResponse.SC_INTERNAL_SERVER_ERROR,
	            new NullError(SchemaConstants.NULLSCHEMA, 
	              "Conditional handling flawed"));
	      }
	    }
	    catch(XCAPException e)
	    {
	      /** Get and send the error codes */
	      if(e.getErrorCode().getError())
	      {
	        //resp.sendError(e.getCode(), e.getErrorCode().serialize());
	        resp.setContentType(e.getErrorCode().getMimeType());
	        resp.setStatus(e.getCode());
	        java.io.PrintWriter pWriter = resp.getWriter();
	        pWriter.print(e.getErrorCode().serialize());
	      }
	      else
	      {
	        resp.setContentType(e.getErrorCode().getMimeType());
	        resp.setStatus(e.getCode());
	        java.io.PrintWriter pWriter = resp.getWriter();
	        pWriter.print(e.getErrorCode().getReason());
	      }
	    } catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}; // catch

	  }; // doPut

	  /**
	   * Method: doPost
	   *         Called by the server (via the service method) to allow a
	   *         servlet to handle a POST request. 
	   *         http://java.sun.com/j2ee/sdk_1.3/techdocs/api/javax/servlet/http/
	   *             HttpServlet.html#doGet(javax.servlet.http.HttpServletRequest,
	   *             javax.servlet.http.HttpServletResponse)
	   * @param  req the HttpServletRequest
	   * @param  resp the HttpServletResponse
	   * @throws ServletException
	   * @throws IOException
	   */
	  protected void doPost(HttpServletRequest req, HttpServletResponse resp) 
	    throws ServletException, IOException 
	  {
	     resp.sendError(HttpServletResponse.SC_METHOD_NOT_ALLOWED, 
	         "POST method not allowed for XCAP");
	  }; // doPost


	  /** 
	   * Method: doDelete
	   *         Called by the server (via the service method) to allow a servlet 
	   *         to handle a DELETE request.
	   *         http://java.sun.com/j2ee/sdk_1.3/techdocs/api/javax/servlet/http/
	   *           HttpServlet.html#doGet(javax.servlet.http.HttpServletRequest,
	   *             javax.servlet.http.HttpServletResponse)
	   * @param request servlet request
	   * @param response servlet response
	   * @throws ServletException
	   * @throws IOException
	   */
	  protected void doDelete(HttpServletRequest req, HttpServletResponse resp)
	    throws ServletException, IOException 
	  {
	    /** 
	     * @NOTE IMPORTANT
	     * The same code as above, please use handlers.
	     * ServiceHandlers have already been implemented
	     */
	    try
	    {
	      disableCache(resp);
	      String docNode = req.getPathInfo();
	      String query = req.getQueryString();

	      if(docNode == null) 
	      {
	        throw new XCAPException(HttpServletResponse.SC_NOT_FOUND, 
	            new NullError(SchemaConstants.NULLSCHEMA, 
	              "XCAP expression not specified"));
	      }

	      if(query == null)
	      {
	    	  LOG.info("NULL QUERY");
	        query = new String("");
	      }
	      
	      DocumentHandler docHandler = new FileDocumentHandler(docNode, query);

	      HttpSemantics httpSemantics = new HttpSemantics();

	      if(httpSemantics.conditionalSemantics(req, resp, docHandler) == true)
	      {
	        docHandler.deleteDocument();
	        resp.setStatus(HttpServletResponse.SC_OK);
	      }
	      else
	      {
	        throw new XCAPException(HttpServletResponse.SC_INTERNAL_SERVER_ERROR,
	            new NullError(SchemaConstants.NULLSCHEMA, 
	              "Conditional handling flawed"));
	      }
	    }
	    catch(XCAPException e)
	    {
	      /** Get and send the error codes */
	      if(e.getErrorCode().getError())
	      {
	        resp.setContentType(e.getErrorCode().getMimeType());
	        resp.setStatus(e.getCode());
	        java.io.PrintWriter pWriter = resp.getWriter();
	        pWriter.print(e.getErrorCode().serialize());
	      }
	      else
	      {
	        resp.setContentType(e.getErrorCode().getMimeType());
	        resp.setStatus(e.getCode());
	        java.io.PrintWriter pWriter = resp.getWriter();
	        pWriter.print(e.getErrorCode().getReason());
	      }
	    } catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}; // catch
	  }; // doDelete


	  /**
	   * Method: disableCache
	   *         Method to disable browser cache
	   */
	  void disableCache(HttpServletResponse resp) 
	  {
	    resp.setHeader("Pragma", "No-cache");
	    resp.setHeader("Cache-Control", "no-cache");
	    resp.setDateHeader("Expires", 0);
	  }; // disableCache

}
